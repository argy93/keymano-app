import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';

class TfaCustomInput extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  singleInputChange(e) {
    const target = e.target;

    if (!target) return;

    const value = target.value;

    if (value === '') {
      return;
    }

    if (value.length > 1) {
      let last_target = target;

      value.split('').forEach((char) => {
        if (char === null || last_target === null) return;

        last_target.value = char;
        last_target = last_target.nextSibling;

        if (last_target !== null) last_target.focus();
      });
    } else {
      const next = target.nextSibling;

      if (next !== null) {
        next.focus();
      }
    }
  }

  singleInputKeyDown(e) {
    const key = e.key;
    const value = e.target.value;
    const prev = e.target.previousSibling;

    if (key === 'Backspace' && value === '') {
      prev ? prev.focus() : '';
    }
  }

  render() {
    const { title, modClass = '' } = this.props;
    return (
      <React.Fragment>
        <div
          className={`c-tfa-custom-input__title ${modClass}`}
        >{title}
        </div>
        <div className="c-tfa-custom-input__field">
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
          <input onChange={this.singleInputChange} onKeyUp={this.singleInputKeyDown} type="text" />
        </div>
      </React.Fragment>
    );
  }
}

TfaCustomInput.propTypes = {
  title: PropTypes.node,
  modClass: PropTypes.string
};

export default TfaCustomInput;
