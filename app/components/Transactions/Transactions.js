import React from 'react';
import './style.scss';
import successImage from './images/success.svg';
import errorImage from './images/error.svg';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';
import ReceiveIcon from '../Icons/ReceiveIcon';
import SendIcon from '../Icons/SendIcon';

class Transactions extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {
      onChangeFilterType,
      onPageChange,
      filterType,
      transactions,
      page,
    } = this.props;

    const items = transactions.filter(transaction => {
      if (filterType === 'all') return true;
      if (filterType === 'deposit' && transaction.amount > 0) return true;
      if (filterType === 'withdraw' && transaction.amount < 0) return true;
    });

    return (
      <div className="c-transactions">
        <div className="c-transactions__head">
          <div className="c-transactions__filter">
            <div onClick={onChangeFilterType.bind(this, 'all')}
                 className={'c-transactions__button' + (filterType === 'all' ? ' is-active' : '')}>
              All
            </div>
            <div onClick={onChangeFilterType.bind(this, 'deposit')}
                 className={'c-transactions__button' + (filterType === 'deposit' ? ' is-active' : '')}>
              Deposit
            </div>
            <div onClick={onChangeFilterType.bind(this, 'withdraw')}
                 className={'c-transactions__button' + (filterType === 'withdraw' ? ' is-active' : '')}>
              Withdraw
            </div>
          </div>
        </div>
        <div className="c-transactions__log">
          <div className="c-transactions__map">
            <span>Status</span>
            <span>Date</span>
            <span>Amount</span>
            <span>Fee</span>
          </div>
          {
            items.slice(page * 3, page * 3 + 3).map(transaction => {
              const t = new Date(transaction.date);
              const formatted = t.format('H:i d M Y');
              const formattedAmount = transaction.amount > 0 ? (<span>+{transaction.amount}</span>) : (
                <span className="is-negative">{transaction.amount}</span>);

              return (
                <div className="c-transactions__item" key={transaction.id}>
                  <div className="c-transactions__item-head">
                    <div className="c-transactions__item-status">
                      {transaction.status === 'success' && transaction.amount > 0 ? (<ReceiveIcon width={'16px'} height={'16px'} />) : ''}
                      {transaction.status === 'success' && transaction.amount < 0 ? (<SendIcon width={'16px'} height={'16px'} />) : ''}
                      {transaction.status === 'error' ? (<img src={errorImage} alt="success"/>) : ''}
                    </div>
                    <div className="c-transactions__item-date">{formatted}</div>
                    <div className="c-transactions__item-amount">{formattedAmount}</div>
                    <div className="c-transactions__item-fee">≈ {transaction.fee}</div>
                  </div>
                  <div className="c-transactions__item-content">
                    <div className="c-transactions__item-cards">
                      <div className="c-transactions__item-label">From</div>
                      {transaction.from.map(fromItem => {
                        return (
                          <div className="c-transactions__item-card" key={fromItem}>{fromItem}</div>
                        );
                      })}
                    </div>
                    <div className="c-transactions__item-cards">
                      <div className="c-transactions__item-label">To</div>
                      {transaction.to.map(toItem => {
                        return (
                          <div className="c-transactions__item-card" key={toItem}>{toItem}</div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              );
            })
          }
        </div>
        {items.length / 3 > 1 ? (
          <ReactPaginate
            previousLabel={'<'}
            nextLabel={'>'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={items.length / 3}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            initialPage={page}
            forcePage={page}
            onPageChange={(page) => {
              onPageChange(page.selected);
            }}
            containerClassName={'c-pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'is-active'}
          />
        ) : ('')}
      </div>
    );
  }
}

Transactions.propTypes = {
  onChangeFilterType: PropTypes.func,
  onPageChange: PropTypes.func,
  filterType: PropTypes.string,
  transactions: PropTypes.array,
  page: PropTypes.number,
};

export default Transactions;
