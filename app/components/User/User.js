import React from 'react';
import './style.scss';
import {Link} from 'react-router-dom';
import UserIcon from "../Icons/UserIcon/UserIcon";

class User extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="c-user">
        <div className="c-user__name">Panteleimon Konstantinopolskiy</div>
        <div className="c-user__icon">
          <Link to='/profile'>
            <UserIcon/>
          </Link>
        </div>
      </div>
    );
  }
}

export default User;
