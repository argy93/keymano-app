import React from 'react';
import copyIcon from './images/copyIcon.svg';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import './style.scss';

class ReceiveLog extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  render() {
    const {items, page, onPageChange} = this.props;

    return (
      <div className="c-receive-log">
        <div className="c-receive-log__head">
          <div className="c-receive-log__status">Used early</div>
          <div className="c-receive-log__address">Address</div>
          <div className="c-receive-log__copy"></div>
        </div>
        <div className="c-receive-log__content">
          {
            items.slice(page * 12, page * 12 + 12).map(item => {
              return (
                <div className="c-receive-log__item" key={item.id}>
                  <div className="c-receive-log__status"><span></span> {item.status}</div>
                  <div className="c-receive-log__address">{item.address}</div>
                  <div className="c-receive-log__copy"><img src={copyIcon} alt=""/></div>
                </div>
              );
            })
          }
        </div>
        {items.length / 12 > 1 ? (
          <ReactPaginate
            previousLabel={'<'}
            nextLabel={'>'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={items.length / 12}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            initialPage={page}
            forcePage={page}
            onPageChange={(page) => {
              onPageChange(page.selected);
            }}
            containerClassName={'c-pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'is-active'}
          />
        ) : ('')}
      </div>
    );
  }
}

ReceiveLog.propTypes = {
  items: PropTypes.array,
  page: PropTypes.number,
  onPageChange: PropTypes.func,
};

export default ReceiveLog;
