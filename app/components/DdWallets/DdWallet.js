import React from 'react';
import './style.scss';
import Select from 'react-dropdown-select';
import PropTypes from 'prop-types';
import WalletTypes from "../../utils/walletTypes";

class DdWallet extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.types = WalletTypes.getTypes();
  }

  render() {
    const {options} = this.props;

    return (
      <div className="c-dd-wallet">
        {
          options.filter(wallet => wallet.id === this.props.active)
            .map(wallet => {
              return (
                <div className="c-dd-wallet__label" key={wallet.id}>
                  My {this.types[wallet.type].name} Wallet
                </div>
              )
            })
        }
        <Select
          contentRenderer={this.customContentRenderer}
          dropdownRenderer={this.customDropdownRenderer}
          options={options}
          autosize={false}
        />
      </div>
    );
  }

  customContentRenderer = ({props, state}) => {
    return (
      <div className="c-dd-wallet__content">
        {
          props.options.filter(wallet => wallet.id === this.props.active)
            .map(wallet => {
              return (<div className="c-dd-wallet__icon" key={wallet.id}>{this.types[wallet.type].resizeIcon('small')}</div>)
            })
        }
      </div>
    )
  };

  customDropdownRenderer = ({props, state, methods}) => {
    const onChange = this.props.onChange;

    return (
      <div className="c-dd-wallet__dropdown">
        {
          props.options.map((wallet) => {
            return (
              <div className="c-dd-wallet__item" onClick={onChange.bind(this, wallet.id)} key={wallet.id}>
                <div className="c-dd-wallet__icon">{this.types[wallet.type].resizeIcon('small')}</div>
                <div className="c-dd-wallet__text">{this.types[wallet.type]['name']}</div>
              </div>
            )
          })
        }
      </div>
    )
  };
}

DdWallet.propTypes = {
  options: PropTypes.array,
  onChange: PropTypes.func,
  active: PropTypes.number
};

export default DdWallet;
