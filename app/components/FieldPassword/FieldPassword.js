import React, {Component} from 'react';
import '../FieldText/style.scss';
import './style.scss';
import {ErrorMessage, Field} from 'formik';
import PropTypes from 'prop-types';
import EyeIcon from '../Icons/EyeIcon';
import PasswordAttention from '../PasswordAttention';

class FieldPassword extends Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      open: true,
      attention: false,
      password: '',
    };

    this.delay = 2000;
    this.attentionDelay = 3000;
    this.timeout = null;
    this.attentionTimeout = null;

    this.closeEye = this.closeEye.bind(this);
    this.openEyeWithTimeout = this.openEyeWithTimeout.bind(this);
    this.inputChange = this.inputChange.bind(this);
  }

  closeEye() {
    clearTimeout(this.timeout);

    this.setState({
      open: false,
    });
  }

  openEyeWithTimeout() {
    this.timeout = setTimeout(() => {
      this.setState({
        open: true,
      });
    }, this.delay);
  }

  inputChange(e) {
    clearTimeout(this.attentionTimeout);

    const value = e.target.value;
    this.setState({
      password: value,
    });

    if (value.length >= 8) {
      this.setState({
        attention: true,
      });

      this.attentionTimeout = setTimeout(() => {
        this.setState({
          attention: false,
        });
      }, this.attentionDelay);
    }
  }

  render() {
    const {
      className = '',
      error,
      touched,
      placeholder,
      name,
      align,
      withAttention,
      onUpdate = () => {},
      text = '',
    } = this.props;
    const {open, attention, password} = this.state;

    return (
      <label
        className={`c-field-password c-field-text is-${align || 'left'} ${(error && touched ? 'is-error' : '')} ${className}`}
        style={{
          zIndex: attention ? 2 : 1,
        }}
      >
        {text ? (<p>{text}</p>) : null}
        <Field
          name={name}
          render={({field, form: {isSubmitting}}) => {
            return (
              <input
                {...field}
                onChange={(e) => {
                  field.onChange(e);
                  this.inputChange(e);
                  onUpdate(e);
                }} type={open ? 'password' : 'text'}
                onKeyUp={(e) => {
                  this.inputChange(e);
                  onUpdate(e);
                }}
                placeholder={placeholder}
              />
            );
          }}
        />
        <ErrorMessage name={name} component="div"/>
        <button
          type="button"
          onMouseDown={this.closeEye}
          onMouseUp={this.openEyeWithTimeout}
        >
          <EyeIcon open={open}/>
        </button>
        {withAttention && attention ? (
          <PasswordAttention password={password}/>
        ) : null}
      </label>
    );
  }
}

FieldPassword.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  withAttention: PropTypes.bool,
  onUpdate: PropTypes.func,
  text: PropTypes.string,
};

export default FieldPassword;
