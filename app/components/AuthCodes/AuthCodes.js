import React from 'react';
import './style.scss';
import DownloadIcon from "../Icons/DownloadIcon";

// eslint-disable-next-line react/prefer-stateless-function
class AuthCodes extends React.Component {
  render() {
    return (
      <div className="c-auth-codes">
        <div className="c-auth-codes__row">
          <div className="c-auth-codes__col is-left">
            <div className="c-auth-codes__col-item">
              <span className="count">1: </span>
              <span className="code">26416d</span>
            </div>
            <div className="c-auth-codes__col-item">
              <span className="count">2: </span>
              <span className="code">b50aa7</span>
            </div>
          </div>
          <div className="c-auth-codes__col is-right">
            <div className="c-auth-codes__col-item">
              <span className="count">3: </span>
              <span className="code">0b8fbb</span>
            </div>
            <div className="c-auth-codes__col-item">
              <span className="count">4: </span>
              <span className="code">ae59db</span>
            </div>
          </div>
        </div>
        <button type="button" className="btn btn-default c-auth-codes__download">
          <DownloadIcon />
          Download PDF
        </button>
      </div>
    );
  }
}

export default AuthCodes;
