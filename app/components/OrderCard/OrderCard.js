import React, {Fragment} from 'react';
import './style.scss';
import blackCard from './images/black-card.png';
import line from './images/line.svg';
import finalStep from './images/card-final-step.svg';
import Button from '../Button';
import PropTypes from 'prop-types';
import {Form, Formik} from 'formik';
import FieldText from '../FieldText';
import FieldSelect from '../FieldSelect';
import Title from '../Title';
import * as Yup from 'yup';
import LoadingIndicator from '../LoadingIndicator';
import Error from '../Error';

const orderSchema = Yup.object().shape({
  firstName: Yup.string()
    .max(64, 'First name to long, maximum 64 symbols')
    .required('First name is required'),
  lastName: Yup.string()
    .max(64, 'Last name to long, maximum 64 symbols')
    .required('Last name is required'),
  email: Yup.string()
    .max(128, 'Email to long, maximum 128 symbols')
    .email('Email not valid')
    .required('Email is required'),
  phone: Yup.string()
    .matches(/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/, (
      <Fragment>Phone number is not valid, <br/> ex. +61 1 2345 6789</Fragment>))
    .required('Phone is required'),
  street: Yup.string()
    .max(128, 'Street name to long, maximum 128 symbols')
    .required('Street is required'),
  apartment: Yup.string()
    .max(128, 'Apartment name to long, maximum 128 symbols')
    .required('Apartment is required'),
  country: Yup.string()
    .required('Country is required'),
  city: Yup.string()
    .max(128, 'City name to long, maximum 128 symbols')
    .required('City is required'),
  zip: Yup.string()
    .max(10, 'ZIP to long, maximum 10 symbols')
    .min(5, 'ZIP to short, minimum 5 symbols')
    .required('ZIP is required'),
});

class OrderCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {
      showOrderForm,
      submitOrderForm,
      orderForm,
      formSending,
      error,
      cardOrdered,
      formData,
    } = this.props;

    return (
      <div className={`c-order-card${orderForm ? ' is-expanded' : ''}`}>
        {!cardOrdered ? (
          <Fragment>
            <div className="c-order-card__default">
              <div className="c-order-card__line"><img src={line} alt=""/></div>
              <div className="c-order-card__img"><img src={blackCard} alt=""/></div>
              <div className="c-order-card__text">
                Get a unique opportunity of using KEYMANO card in<br/> your casual life.
              </div>
              {!orderForm ? (
                <Button
                  style={'filled'}
                  onClick={showOrderForm}
                >
                  Order Keymano card
                </Button>
              ) : null}
            </div>
            {orderForm ? (
              <div className={`c-order-card__form${formSending ? ' is-sending' : ''}`}>
                {formSending ? (
                  <LoadingIndicator/>
                ) : null}
                <Title size={'medium'}>Shipping address</Title>
                <Formik
                  initialValues={formData}
                  validateOnChange={true}
                  validationSchema={orderSchema}
                  onSubmit={(values, {setSubmitting}) => {
                    submitOrderForm(values);
                    setSubmitting(false);
                  }}
                >
                  {({isSubmitting, touched, errors}) => (
                    <Form>
                      <div className="c-order-card__row">
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.firstName}
                            touched={touched.firstName}
                            name={'firstName'}
                            placeholder={''}
                            text={'First name'}
                          />
                        </div>
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.lastName}
                            touched={touched.lastName}
                            name={'lastName'}
                            placeholder={''}
                            text={'Last name'}
                          />
                        </div>
                      </div>
                      <div className="c-order-card__row">
                        <div className="c-order-card__column">
                          <FieldText
                            type={'email'}
                            error={errors.email}
                            touched={touched.email}
                            name={'email'}
                            placeholder={''}
                            text={'E-mail'}
                          />
                        </div>
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.phone}
                            touched={touched.phone}
                            name={'phone'}
                            placeholder={'+61 1 2345 6789'}
                            text={'Phone number'}
                          />
                        </div>
                      </div>
                      <div className="c-order-card__row">
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.street}
                            touched={touched.street}
                            name={'street'}
                            placeholder={'House number and the street name'}
                            text={'Street address'}
                          />
                        </div>
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.apartment}
                            touched={touched.apartment}
                            name={'apartment'}
                            placeholder={'Apartment, suite, unit (etc.)'}
                            text={'Apartment'}
                          />
                        </div>
                      </div>
                      <div className="c-order-card__row">
                        <div className="c-order-card__column">
                          <FieldSelect
                            error={errors.country}
                            touched={touched.country}
                            name={'country'}
                            text={'Country'}
                            options={[
                              {value: '1', text: 'Germany'},
                              {value: '2', text: 'Ukraine'},
                            ]}
                          />
                        </div>
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.city}
                            touched={touched.city}
                            name={'city'}
                            placeholder={''}
                            text={'Town / City'}
                          />
                        </div>
                      </div>
                      <div className="c-order-card__row">
                        <div className="c-order-card__column">
                          <FieldText
                            error={errors.zip}
                            touched={touched.zip}
                            name={'zip'}
                            placeholder={''}
                            text={'ZIP'}
                          />
                        </div>
                        <div className="c-order-card__column"/>
                      </div>
                      {error ? (
                        <Error>
                          Something went wrong. Try again later.
                        </Error>
                      ) : null}
                      <Button
                        style={'filled'}
                        type={'submit'}
                        disabled={Boolean(formSending || isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                      >
                        ORDER KEYMANO CARD
                      </Button>
                    </Form>
                  )}
                </Formik>
              </div>
            ) : null}
          </Fragment>
        ) : (
          <div className="c-order-card__default is-final">
            <div className="c-order-card__line"><img src={line} alt=""/></div>
            <div className="c-order-card__img"><img src={finalStep} alt=""/></div>
            <div className="c-order-card__text">
              Request has been sent. We have sent you the details<br/>
              of your delivery on your e-mail.
            </div>
          </div>
        )}
      </div>
    );
  }
}

OrderCard.propTypes = {
  showOrderForm: PropTypes.func,
  submitOrderForm: PropTypes.func,
  orderForm: PropTypes.bool,
  formSending: PropTypes.bool,
  error: PropTypes.bool,
  cardOrdered: PropTypes.bool,
  formData: PropTypes.object,
};

export default OrderCard;
