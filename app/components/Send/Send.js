import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import {Form, Formik} from 'formik';
import FieldText from '../FieldText';
import Button from '../Button';
import * as Yup from 'yup';

const sendSchema = Yup.object().shape({
  btc: Yup.number('BTC must be a number')
    .required('BTC is required'),
  usd: Yup.number('USD must be a number')
    .required('USD is required'),
  eur: Yup.number('EUR must be a number')
    .required('EUR is required'),
  address: Yup.string()
    .required('Address is required'),
});

class Send extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {loadCurrencies} = this.props;

    loadCurrencies();
  }

  render() {
    const {currencies} = this.props;

    return (
      <div className="c-send">
        <div className="c-send__form">
          <Formik
            // initialValues={{firstName, lastName}}
            validateOnChange={true}
            validationSchema={sendSchema}
            onSubmit={(values, {setSubmitting}) => {

            }}
          >
            {({isSubmitting, touched, errors}) => (
              <Form>
                <div className="c-send__label">Amount</div>
                <div className="c-send__row c-send__amount">
                  <div>
                    <FieldText
                      name="btc"
                      touched={touched.btc}
                      error={errors.btc}
                      atopPlaceholder={'BTC'}
                      onChange={(e, field, form) => {
                        const value = parseFloat(e.target.value);

                        if (isNaN(value)) {
                          form.setFieldValue('usd', '');
                          form.setFieldValue('eur', '');
                        } else {
                          const usd = (value * currencies.btcusd).toFixed(2);
                          const eur = (value * currencies.btceur).toFixed(2);

                          form.setFieldValue('usd', usd);
                          form.setFieldValue('eur', eur);
                        }
                      }}
                    />
                  </div>
                  <div>
                    <FieldText
                      name="usd"
                      touched={touched.usd}
                      error={errors.usd}
                      atopPlaceholder={'USD'}
                      onChange={(e, field, form) => {
                        const value = parseFloat(e.target.value);

                        if (isNaN(value)) {
                          form.setFieldValue('btc', '');
                          form.setFieldValue('eur', '');
                        } else {
                          const btc = (value / currencies.btcusd).toFixed(6);
                          const eur = (btc * currencies.btceur).toFixed(2);

                          form.setFieldValue('btc', btc);
                          form.setFieldValue('eur', eur);
                        }
                      }}
                    />
                  </div>
                  <div>
                    <FieldText
                      name="eur"
                      touched={touched.eur}
                      error={errors.eur}
                      atopPlaceholder={'EUR'}
                      onChange={(e, field, form) => {
                        const value = parseFloat(e.target.value);

                        if (isNaN(value)) {
                          form.setFieldValue('btc', '');
                          form.setFieldValue('usd', '');
                        } else {
                          const btc = (value / currencies.btceur).toFixed(6);
                          const usd = (btc * currencies.btcusd).toFixed(2);

                          form.setFieldValue('btc', btc);
                          form.setFieldValue('usd', usd);
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="c-send__label">Receiving address</div>
                <div className="c-send__row c-send__address">
                  <FieldText
                    name={'address'}
                    touched={touched.address}
                    error={errors.address}
                  />
                </div>
                <div className="c-send__row is-foot">
                  <Button
                    type={'submit'}
                    style={'filled'}
                    disabled={Boolean(isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                  >
                    Send
                  </Button>
                  <div className="c-send__info-item">Fee: ≈0.0003 BTC</div>
                  <div className="c-send__info-item">Send up to: ≈23.994 BTC</div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

Send.propTypes = {
  loadCurrencies: PropTypes.func,
  loadingCurrencies: PropTypes.bool,
  loadCurrenciesError: PropTypes.bool,
  loadCurrenciesErrorMessage: PropTypes.string,
  currencies: PropTypes.object,
};

export default Send;
