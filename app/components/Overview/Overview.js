import React from 'react';
import FilterCardsIcon from "../Icons/FilterCardsIcon";
import FilterLinesIcon from "../Icons/FilterLinesIcon";
import PropTypes from 'prop-types';
import './style.scss';

class Overview extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {title, active, onCardsClick, onShortClick} = this.props;

    const cardIcon = active === 'cards' ? (<FilterCardsIcon active={true}/>) : (<FilterCardsIcon/>);
    const shortIcon = active === 'short' ? (<FilterLinesIcon active={true}/>) : (<FilterLinesIcon/>);

    return (
      <div className="c-overview">
        <div className="c-overview__title">{title}</div>
        <div className="c-overview__toggle">
          <div className="c-overview__button c-overview__cards" onClick={onCardsClick}>{cardIcon}</div>
          <div className="c-overview__button c-overview__short" onClick={onShortClick}>{shortIcon}</div>
        </div>
      </div>
    );
  }
}

Overview.propTypes = {
  title: PropTypes.string,
  active: PropTypes.oneOf(['cards', 'short']),
  onCardsClick: PropTypes.func,
  onShortClick: PropTypes.func
};

export default Overview;
