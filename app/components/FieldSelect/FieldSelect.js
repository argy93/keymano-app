import React, {Component} from 'react';
import '../FieldText/style.scss';
import './style.scss';
import {ErrorMessage, Field} from 'formik';
import PropTypes from 'prop-types';

class FieldSelect extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      className = '',
      error,
      touched,
      name,
      align = 'left',
      text = '',
      options,
    } = this.props;
    return (
      <label
        className={`c-field-select c-field-text is-${align} ${(error && touched ? 'is-error' : '')} ${className}`}>
        {text ? (<p>{text}</p>) : null}
        <Field component="select" name={name}>
          <option disabled value="">Select country</option>
          {options.map((option) => (
            <option value={option.value} key={option.value}>{option.text}</option>
          ))}
        </Field>
        <ErrorMessage name={name} component="div"/>
      </label>
    );
  }
}

FieldSelect.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
  name: PropTypes.string,
  text: PropTypes.string,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    text: PropTypes.string,
  })),
};

export default FieldSelect;
