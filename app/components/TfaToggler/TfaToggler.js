import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';

// eslint-disable-next-line react/prefer-stateless-function
class TfaToggler extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {title, onToggleClick, tfa} = this.props;

    return (
      <div className="c-auth-buttons">
        <p className="c-auth-buttons__title">{title}</p>
        <div className="c-auth-buttons__controls">
          <button onClick={onToggleClick.bind(null, 'totp')} type="button"
                  className={"c-auth-buttons__controls-btn" + (tfa === 'totp' ? ' is-active': '')}>TOTP
          </button>
          <button onClick={onToggleClick.bind(null, 'paper')} type="button"
                  className={"c-auth-buttons__controls-btn" + (tfa === 'paper' ? ' is-active': '')}>Paper
          </button>
          <button onClick={onToggleClick.bind(null, 'none')} type="button"
                  className={"c-auth-buttons__controls-btn" + (tfa === 'none' ? ' is-active': '')}>None
          </button>
        </div>
      </div>
    );
  }
}

TfaToggler.propTypes = {
  title: PropTypes.string,
  onToggleClick: PropTypes.func,
  tfa: PropTypes.oneOf(['totp', 'paper', 'none']),
};

export default TfaToggler;
