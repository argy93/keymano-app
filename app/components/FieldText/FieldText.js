import React, {Component} from 'react';
import './style.scss';
import {ErrorMessage, Field} from 'formik';
import PropTypes from 'prop-types';

class FieldText extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      className = '',
      error,
      touched,
      placeholder,
      atopPlaceholder,
      name,
      type,
      align = 'left',
      text = '',
      onChange = () => {},
    } = this.props;
    return (
      <label
        className={`c-field-text is-${align} ${(error && touched ? 'is-error' : '')} ${className}`}>
        {text ? (<p>{text}</p>) : null}
        {atopPlaceholder ? (<div className="c-field-text__atop-placeholder">{atopPlaceholder}</div>) : null}
        <Field
          type={type || 'text'}
          render={({field, form}) => (
            <input
              {...field}
              name={name}
              placeholder={placeholder}
              onChange={(e) => {
                field.onChange(e);
                onChange(e, field, form);
              }}
            />
          )}
          name={name}
          placeholder={placeholder}
        />
        <ErrorMessage name={name} component="div"/>
      </label>
    );
  }
}

FieldText.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
  placeholder: PropTypes.string,
  atopPlaceholder: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  text: PropTypes.string,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  onChange: PropTypes.func,
};

export default FieldText;
