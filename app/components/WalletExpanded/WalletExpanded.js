import React from 'react';
import './style.scss';

import PropTypes from "prop-types";
import WalletTypes from "../../utils/walletTypes";

class WalletExpanded extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.types = WalletTypes.getTypes();
  }

  render() {
    const {
      summary,
      type,
      eq,
      code
    } = this.props;

    return (
      <div className="c-wallet-expanded">
        <div className="c-wallet-expanded__label">Balance:</div>
        <div className="c-wallet-expanded__summ">{summary} <span>{this.types[type].short}</span></div>
        <div className="c-wallet-expanded__eq">{eq} eur</div>
        <div className="c-wallet-expanded__foot">
          <div className="c-wallet-expanded__label">Your Bitcoin adress:</div>
          <a className="c-wallet-expanded__code-label">QR-code</a>
          <div className="c-wallet-expanded__code">{code}</div>
        </div>
      </div>
    );
  }
}

WalletExpanded.propTypes = {
  summary: PropTypes.number,
  type: PropTypes.string,
  eq: PropTypes.number,
  code: PropTypes.string
};

export default WalletExpanded;
