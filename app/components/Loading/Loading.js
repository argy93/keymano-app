import React, {Component} from 'react';
import PropTypes from 'prop-types';
import LoadingIndicator from '../LoadingIndicator';
import './style.scss';

class Loading extends Component {

  render() {
    const {
      children,
      showLoader
    } = this.props;

    return (
      <div className={`c-loading${showLoader ? ' is-active': null}`}>
        <div className="c-loading__content">
          {children}
        </div>
        {showLoader ? (
          <LoadingIndicator/>
        ): null}
      </div>
    )
  }
}

Loading.propTypes = {
  showLoader: PropTypes.bool,
  children: PropTypes.any,
};

export default Loading;
