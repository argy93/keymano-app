import React, {Component} from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import Sidebar from '../Sidebar';
import Header from '../Header';

class Dashboard extends Component {
  render() {
    const {children} = this.props;

    return (
      <div className="c-dashboard">
        <Sidebar/>
        <div className="c-dashboard__content">
          <Header />
          {children}
        </div>
      </div>
    );
  }
}

export default Dashboard;
