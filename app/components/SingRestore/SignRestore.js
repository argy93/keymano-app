import React, {Fragment} from 'react';
import {Link, withRouter} from 'react-router-dom';
import '../SingIn/style.scss';
import './style.scss';
import {Formik, Form} from 'formik';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import FieldText from '../FieldText';
import Button from '../Button';
import Title from '../Title';
import successImage from './images/resetSuccess.svg';
import Error from '../Error';
import Loading from '../Loading';

const restoreSchema = Yup.object().shape({
  email: Yup.string()
    .max(128, 'Email to long, maximum 128 symbols')
    .email('Email not valid')
    .required('Email is required'),
});

class SignRestore extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {
      sendRestorePassword,
      restoreSending,
      restoreSendingError,
      restoreSendingErrorMessage,
      restoreSendingSuccess,
      formData,
      history
    } = this.props;

    return (
      <div className="c-sign-restore c-sign-in">
        {
          !restoreSendingSuccess ? (
            <Fragment>
              <Title>
                Enter your email address below to get<br/>
                instructions for setting a new password.
              </Title>
              <Loading
                showLoader={restoreSending}
              >
                <div className="c-sign-in__form">
                <Formik
                  initialValues={{...formData}}
                  validateOnChange={true}
                  validationSchema={restoreSchema}
                  onSubmit={(values, {setSubmitting}) => {
                    sendRestorePassword(values.email);
                  }}
                >
                  {({isSubmitting, touched, errors}) => (
                    <Form>
                      <FieldText
                        name={'email'}
                        placeholder={'Email'}
                        error={errors.email}
                        touched={touched.email}
                      />

                      {restoreSendingError ? (
                        <Error>
                          Something went wrong
                        </Error>
                      ): null}

                      <Button
                        type={'submit'}
                        style={'red'}
                        disabled={Boolean(isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                      >RESET PASSWORD</Button>

                      <div className="c-sign-in__margin-row is-small">
                        Already have an account? <Link to='/sign-in'>Sign in</Link>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
              </Loading>
            </Fragment>
          ) : (
            <Fragment>
              <div className="c-sign-restore__success">
                <div className="c-sign-restore__image">
                  <img src={successImage} alt=""/>
                </div>
                <Title>Password reset email sent!</Title>
                <p>
                  An email has been sent to your email adress, {formData.email}.
                  Follow the directions in the email to reset your password.
                </p>
                <div className="c-sign-restore__resend">
                  Didn’t get the e-mail? You can <a>resend it again.</a>
                </div>
                <Button
                  style={'default'}
                  onClick={() => {
                      history.push('/sign-in')
                  }}
                >
                  Get back to log in page
                </Button>
              </div>
            </Fragment>
          )
        }
      </div>
    );
  }
}

SignRestore.propTypes = {
  sendRestorePassword: PropTypes.func,
  restoreSending: PropTypes.bool,
  restoreSendingError: PropTypes.bool,
  restoreSendingErrorMessage: PropTypes.string,
  restoreSendingSuccess: PropTypes.bool,
  formData: PropTypes.object,
};

export default withRouter(SignRestore);
