import React, {Component} from 'react';
import './style.scss';
import PropTypes from 'prop-types';

class Title extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      children,
      size = 'large',
    } = this.props;

    return (
      <div className={`c-title is-${size}`}>{children}</div>
    );
  }
}

Title.propTypes = {
  size: PropTypes.oneOf(['large', 'medium']),
};

export default Title;
