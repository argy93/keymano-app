import React from 'react';
import './style.scss';
import AuthCodes from '../AuthCodes';
import TfaCustomInput from '../TfaCustomInput';

class PaperSetup extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="c-auth-form">
        <div className="c-auth-form__header">
          <p className="c-auth-form__header-message">Authentification Codes to save</p>
        </div>
        <div className="c-auth-form__body">

          <AuthCodes />
          <TfaCustomInput
            title={'Enter one of saved codes'}
          />
          <button type="button" className="btn btn-primary">Enable Paper Codes</button>
        </div>
      </div>
    );
  }
}

export default PaperSetup;
