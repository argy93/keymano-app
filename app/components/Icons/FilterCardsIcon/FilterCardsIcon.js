import React from 'react';
import PropTypes from 'prop-types';

const FilterCardsIcon = ({
  width = 16, height = 13, className, active = false
}) => {
  const color = active ? '#FF2800' : '#C0C0C0';

  return (
    <svg width={width} height={height} className={className} viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M0 6H4.70588V0H0V6ZM0 13H4.70588V7H0V13ZM5.64706 13H10.3529V7H5.64706V13ZM11.2941 13H16V7H11.2941V13ZM5.64706 6H10.3529V0H5.64706V6ZM11.2941 0V6H16V0H11.2941Z" fill={color} />
    </svg>
  );
};

FilterCardsIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  active: PropTypes.bool
};

export default FilterCardsIcon;
