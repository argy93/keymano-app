import React from 'react';
import PropTypes from 'prop-types';

const RippleIcon = ({
  // eslint-disable-next-line react/prop-types
  width = 32, height = 26, className, type
}) => {
  if (type === 'small') {
    width = 18;
    height = 26;
  }

  return (
    <svg width={width} height={height} className={className} viewBox="0 0 30 26" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M25.6447 0H30L20.9389 9.35638C17.6589 12.7433 12.341 12.7433 9.06103 9.35638L0 0H4.35521L11.2387 7.10781C13.316 9.25283 16.6839 9.25283 18.7613 7.10781L25.6447 0Z" fill="#1D1D1D" />
      <path fillRule="evenodd" clipRule="evenodd" d="M4.33903 25.8621H0L9.08312 16.4935C12.3509 13.123 17.6491 13.123 20.9169 16.4935L30 25.8621H25.6609L18.7473 18.7311C16.6776 16.5964 13.3222 16.5964 11.2526 18.7311L4.33903 25.8621Z" fill="#1D1D1D" />
    </svg>
  );
};

RippleIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default RippleIcon;
