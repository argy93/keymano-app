import React from 'react';
import PropTypes from 'prop-types';

const CheckIcon = ({width = 20, height = 20, className}) => {
  return (
    <svg width={width} height={height} className={className} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="20" height="20" rx="10" fill="#41B3C2"/>
      <path d="M5 9.43243L8.72881 13L15 7" stroke="white" strokeWidth="2" strokeLinecap="round"/>
    </svg>
  );
};

CheckIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default CheckIcon;
