import React from 'react';
import PropTypes from 'prop-types';

const CardsIcon = ({
  width = 24, height = 24, className
}) => {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <mask id="cardsIconMask0" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
        <path fillRule="evenodd" clipRule="evenodd" d="M0 0H24V24H0V0Z" fill="white"/>
      </mask>
      <g mask="url(#cardsIconMask0)">
        <path fillRule="evenodd" clipRule="evenodd" d="M2 19.9997H22C23.1045 19.9997 24 19.1042 24 17.9997V5.99996C24 4.89548 23.1045 4 22 4H2C0.8955 4 0 4.89548 0 5.99996V17.9997C0 19.1042 0.8955 19.9997 2 19.9997ZM21.9998 17.9997H1.99977V5.99998H21.9998V17.9997Z" fill="#4A4A4A"/>
        <path fillRule="evenodd" clipRule="evenodd" d="M5.99392 7.99951H4.59408C4.00576 7.99951 3.5293 8.47597 3.5293 9.0643V10.4641C3.5293 11.0519 4.00576 11.5289 4.59408 11.5289H5.99392C6.58172 11.5289 7.05871 11.0519 7.05871 10.4641V9.0643C7.05871 8.47597 6.58172 7.99951 5.99392 7.99951Z" fill="#4A4A4A"/>
        <path fillRule="evenodd" clipRule="evenodd" d="M7.37243 13.6465H4.62734C4.02122 13.6465 3.5293 14.1208 3.5293 14.7053C3.5293 15.2898 4.02122 15.7641 4.62734 15.7641H7.37243C7.9791 15.7641 8.47047 15.2898 8.47047 14.7053C8.47047 14.1208 7.9791 13.6465 7.37243 13.6465Z" fill="#4A4A4A"/>
        <path fillRule="evenodd" clipRule="evenodd" d="M13.0189 13.6465H10.2738C9.6677 13.6465 9.17578 14.1208 9.17578 14.7053C9.17578 15.2898 9.6677 15.7641 10.2738 15.7641H13.0189C13.6256 15.7641 14.117 15.2898 14.117 14.7053C14.117 14.1208 13.6256 13.6465 13.0189 13.6465Z" fill="#4A4A4A"/>
        <path fillRule="evenodd" clipRule="evenodd" d="M18.6674 13.6465H15.9223C15.3161 13.6465 14.8242 14.1208 14.8242 14.7053C14.8242 15.2898 15.3161 15.7641 15.9223 15.7641H18.6674C19.274 15.7641 19.7654 15.2898 19.7654 14.7053C19.7654 14.1208 19.274 13.6465 18.6674 13.6465Z" fill="#4A4A4A"/>
      </g>
    </svg>

  );
};

CardsIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default CardsIcon;
