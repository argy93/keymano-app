import React from 'react';
import PropTypes from 'prop-types';

const QRCodeIcon = ({ width = 19, height = 19, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M0 5.54159V0H5.54159V5.54159H0ZM1.58341 3.95841H3.95841V1.58341H1.58341V3.95841Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M0 19V13.4581H5.54159V19H0ZM1.58341 17.4168H3.95841V15.0418H1.58341V17.4168Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M13.4585 5.54159V0H19.0001V5.54159H13.4585ZM15.0417 3.95841H17.4167V1.58341H15.0417V3.95841Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M17.4167 7.125V11.875H13.4585V13.4582H19.0001V7.125H17.4167Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M13.4585 15.0419V19H15.0417V16.625H17.4167V19H19.0001V15.0419H13.4585Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M7.125 0V1.58341H10.2916V5.54159H11.875V0H7.125Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M10.2916 7.125V10.2918H7.125V15.0418H10.2916V19H11.875V13.4582H8.70841V11.875H11.875V8.70841H13.4584V10.2918H15.0416V7.125H10.2916Z" />
    <rect x="7.125" y="16.625" width="1.58341" height="2.375" />
    <rect x="3.1665" y="10.2919" width="2.375" height="1.58317" />
    <path fillRule="evenodd" clipRule="evenodd" d="M7.125 3.16663V7.12504H0V11.875H1.58341V8.70845H8.70841V3.16663H7.125Z" />
  </svg>
);

QRCodeIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default QRCodeIcon;
