import React from 'react';
import { shallow } from 'enzyme';

import LogoIcon from '../index';

describe('<LogoIcon />', () => {
  it('should render a SVG', () => {
    const renderedComponent = shallow(<LogoIcon />);
    expect(renderedComponent.find('svg').length).toBe(1);
  });
});
