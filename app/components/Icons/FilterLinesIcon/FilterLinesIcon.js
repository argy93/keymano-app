import React from 'react';
import PropTypes from 'prop-types';

const FilterLinesIcon = ({
  width = 16, height = 13, className, active = false
}) => {
  const color = active ? '#FF2800' : '#C0C0C0';

  return (
    <svg width={width} height={height} className={className} viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M0 13H16V7H0V13ZM0 0V6H16V0H0Z" fill={color} />
    </svg>
  );
};

FilterLinesIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  active: PropTypes.bool
};

export default FilterLinesIcon;
