import React from 'react';
import PropTypes from 'prop-types';

const TransactionsIcon = ({
  width = 18, height = 18, className
}) => {
  return (
    <svg width={width} height={height} className={className} viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#transactionIconclip0)">
        <mask id="transactionIconMask0" maskUnits="userSpaceOnUse" x="2" y="-1" width="14" height="11">
          <path fillRule="evenodd" clipRule="evenodd" d="M2.46875 -0.120117H15.682V9.54208H2.46875V-0.120117Z" fill="white"/>
        </mask>
        <g mask="url(#transactionIconMask0)">
          <path fillRule="evenodd" clipRule="evenodd" d="M15.4019 5.37776C15.5756 5.2046 15.6825 4.96519 15.6825 4.71274C15.6825 4.45978 15.5887 4.22037 15.4019 4.0472L11.4874 0.149411C11.127 -0.20996 10.5251 -0.20996 10.1647 0.149411C9.8043 0.508782 9.8043 1.10704 10.1647 1.46641L12.4764 3.7812H3.40395C2.88289 3.7812 2.46875 4.19377 2.46875 4.71274C2.46875 5.23172 2.88289 5.64377 3.40395 5.64377H12.4764L10.1647 7.94552C9.8043 8.30489 9.8043 8.90367 10.1647 9.26252C10.3514 9.44924 10.5919 9.54208 10.8329 9.54208C11.0733 9.54208 11.3138 9.44924 11.501 9.26252L15.4019 5.37776Z" fill="#333333"/>
        </g>
        <mask id="transactionIconMask1" maskUnits="userSpaceOnUse" x="2" y="8" width="14" height="10">
          <path fillRule="evenodd" clipRule="evenodd" d="M2.46875 8.33789H15.682V18.0001H2.46875V8.33789Z" fill="white"/>
        </mask>
        <g mask="url(#transactionIconMask1)">
          <path fillRule="evenodd" clipRule="evenodd" d="M2.74936 12.5022C2.57567 12.6754 2.46875 12.9148 2.46875 13.1672C2.46875 13.4202 2.56263 13.6596 2.74936 13.8328L6.66385 17.7306C7.02426 18.0899 7.62617 18.0899 7.98658 17.7306C8.34699 17.3717 8.34699 16.7729 7.98658 16.4136L5.67492 14.0988H14.7473C15.2684 14.0988 15.6825 13.6862 15.6825 13.1672C15.6825 12.6483 15.2684 12.2362 14.7473 12.2362H5.67492L7.98658 9.93446C8.34699 9.57509 8.34699 8.97631 7.98658 8.61746C7.79985 8.43073 7.55888 8.33789 7.31843 8.33789C7.07798 8.33789 6.83753 8.43073 6.65028 8.61746L2.74936 12.5022Z" fill="#333333"/>
        </g>
      </g>
      <defs>
        <clipPath id="transactionIconclip0">
          <rect width="18" height="18" fill="white"/>
        </clipPath>
      </defs>
    </svg>

  );
};

TransactionsIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default TransactionsIcon;
