import React from 'react';
import PropTypes from 'prop-types';

const BackIcon = ({ width = 6, height = 9, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M5.41 7.7317L2.06617 4.38057L5.41 1.02943L4.38057 0L0 4.38057L4.38057 8.76113L5.41 7.7317Z" fill="#1D2024" />
  </svg>
);

BackIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default BackIcon;
