import React from 'react';
import PropTypes from 'prop-types';

const StopIcon = ({width = 20, height = 20, className}) => {
  return (
    <svg width={width} height={height} className={className} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="20" height="20" rx="10" fill="#FF2600"/>
      <path d="M6 10H14" stroke="white" strokeWidth="2" strokeLinecap="round"/>
    </svg>

  );
};

StopIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default StopIcon;
