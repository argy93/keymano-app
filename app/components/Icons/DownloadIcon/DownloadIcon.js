import React from 'react';
import PropTypes from 'prop-types';

const DownloadIcon = ({ width = 15, height = 20, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 15 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M6.00637 1.08692V12.5196L3.71082 10.214C3.33071 9.79826 2.70167 9.7847 2.30566 10.1832C1.90966 10.5823 1.89674 11.2427 2.27684 11.6584C2.29672 11.6803 2.31759 11.7012 2.33945 11.7215L6.31443 15.7121C6.69852 16.096 7.30172 16.096 7.6858 15.7121L11.6608 11.7215C12.0737 11.3418 12.1154 10.683 11.7542 10.249C11.393 9.81547 10.7649 9.77166 10.352 10.1509C10.3302 10.1712 10.3093 10.1921 10.2894 10.214L7.99386 12.5087V1.03267C7.98741 0.349328 7.35538 -0.175439 6.67218 0.0551249C6.26027 0.194402 6.00637 0.632577 6.00637 1.08692Z" fill="#333333" />
    <path fillRule="evenodd" clipRule="evenodd" d="M14 18H1C0.448 18 0 18.448 0 19C0 19.552 0.448 20 1 20H14C14.552 20 15 19.552 15 19C15 18.448 14.552 18 14 18Z" fill="#333333" />
  </svg>
);

DownloadIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default DownloadIcon;
