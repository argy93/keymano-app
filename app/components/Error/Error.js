import React, {Component} from 'react';
import './style.scss';

export default class Error extends Component {
  render() {
    return (
      <div className="c-error">
        { this.props.children }
      </div>
    )
  }
}
