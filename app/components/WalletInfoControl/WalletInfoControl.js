import React from 'react';
import './style.scss';

import PropTypes from "prop-types";
import SendIcon from "../Icons/SendIcon/SendIcon";
import ReceiveIcon from "../Icons/ReceiveIcon/ReceiveIcon";
import TransactionsIcon from "../Icons/TransactionsIcon/TransactionsIcon";

class WalletInfoControl extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {
      filterType,
      changeFilterType
    } = this.props;

    return (
      <div className="c-wallet-info-control">
        <div className={'c-wallet-info-control__button is-short is-blue' + (filterType === 'receive' ? ' is-active' : '')}
             onClick={changeFilterType.bind(this, 'receive')}>
          <ReceiveIcon/>
          Receive
        </div>
        <div className={'c-wallet-info-control__button is-short is-orange' + (filterType === 'send' ? ' is-active' : '')}
             onClick={changeFilterType.bind(this, 'send')}>
          <SendIcon/>
          Send
        </div>
        <div className={'c-wallet-info-control__button is-wide' + (filterType === 'transactions' ? ' is-active' : '')}
             onClick={changeFilterType.bind(this, 'transactions')}>
          <TransactionsIcon/>
          Transactions
        </div>
      </div>
    );
  }
}

WalletInfoControl.propTypes = {
  filterType: PropTypes.oneOf(['transactions', 'receive', 'send']),
  changeFilterType: PropTypes.func
};

export default WalletInfoControl;
