import React from 'react';
import './style.scss';

import PropTypes from "prop-types";
import WalletTypes from "../../utils/walletTypes";
import Link from 'react-router-dom/Link';

class Wallet extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.types = WalletTypes.getTypes();
  }

  render() {
    const {
      id,
      type,
      date,
      eq,
      code,
      shift,
      summary,
      fee,
      mode,
      onClick,
      to
    } = this.props;

    const t = new Date(date);
    const formatted = t.format("H:i d M Y");

    const prepared_shift = shift > 0 ?
      (<span className="c-wallet__green">+{shift} {this.types[type].short}</span>) :
      (<span className="c-wallet__red">{shift} {this.types[type].short}</span>);

    return (
      <Link to={to} className={'c-wallet' + (mode === 'short' ? ' is-short': '')}>
        <div className="c-wallet__head">{this.types[type].icon} {this.types[type].name}</div>
        <div className="c-wallet__info">
          <div className="c-wallet__summary">{summary} {this.types[type].short}</div>
          <div className="c-wallet__eq">{eq} EUR</div>
          <div className="c-wallet__fee">Fee: ≈ {fee} {this.types[type].short}</div>
        </div>
        <div className="c-wallet__additional">
          <div className="c-wallet__additional-head">
            <div className="c-wallet__date">{formatted}</div>
            <div className="c-wallet__summ">{prepared_shift}</div>
          </div>
          <div className="c-wallet__transaction">{code}</div>
        </div>
      </Link>
    );
  }
}

Wallet.propTypes = {
  id: PropTypes.number,
  type: PropTypes.oneOf(['bitcoin', 'etherium', 'ripple', 'litecoin', 'eosio']),
  summary: PropTypes.number,
  eq: PropTypes.number,
  date: PropTypes.number,
  shift: PropTypes.number,
  code: PropTypes.string,
  fee: PropTypes.number,
  mode: PropTypes.string,
  onClick: PropTypes.func
};

export default Wallet;
