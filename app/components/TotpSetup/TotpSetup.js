import React from 'react';
import './style.scss';
import ManuallyIcon from '../Icons/ManuallyIcon';
import CopyIcon from '../Icons/CopyIcon';
import QRCodeIcon from '../Icons/QRCodeIcon';
import QRCodeImage from '../Icons/QRCodeImage';
import TfaCustomInput from '../TfaCustomInput';

class TotpSetup extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = { setupType: 'manually' };
  }

  onSetupTypeChange(type) {
    this.setState({ setupType: type });
  }

  render() {
    const tfaCustomInputTitle = (
      <React.Fragment>
        Enter 2FA TOTP Code from <span>Google Authentificator</span> mobile application
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <p className="l-settings__info">
          Enter account and key in
          <span> Google Authentificator </span>
          mobile application manually or just scan QR code
        </p>
        <div className="c-auth-form">
          <div className="c-auth-form__header">
            <div
              onClick={this.onSetupTypeChange.bind(this, 'manually')}
              className={`c-auth-form__header-item${this.state.setupType === 'manually' ? ' is-active' : ''}`}
            >
              <ManuallyIcon />
              <span>Manually</span>
            </div>
            <div
              onClick={this.onSetupTypeChange.bind(this, 'qr')}
              className={`c-auth-form__header-item${this.state.setupType === 'qr' ? ' is-active' : ''}`}
            >
              <QRCodeIcon />
              <span>QR code</span>
            </div>
          </div>
          <div className="c-auth-form__body">

            {this.state.setupType === 'manually' ? (
              <div className="input-group">
                <div className="input-group__item">
                  <input
                    type="email"
                    className="c-auth-form__body-input"
                    placeholder="E-mail"
                  />
                  <CopyIcon className="c-copy-svg" />
                </div>
                <div className="input-group__item">
                  <input
                    type="text"
                    className="c-auth-form__body-input"
                    placeholder="Key (click to copy)"
                  />
                  <CopyIcon className="c-copy-svg" />
                </div>
              </div>
            ) : ''}
            {this.state.setupType === 'qr' ? (
              <QRCodeImage />
            ) : ''}
            <TfaCustomInput
              title={tfaCustomInputTitle}
            />
            <button type="button" className="btn btn-primary">Enable TOTP</button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default TotpSetup;
