import React from 'react';
import './style.scss';
import PlusIcon from '../Icons/PlusIcon';

class AddWallet extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  render() {
    const {onAddWallet} = this.props;

    return (
      <div className="c-add-wallet" onClick={onAddWallet}>
        <div className="c-add-wallet__icon"><PlusIcon/></div>
        <div className="c-add-wallet__text">Add wallet</div>
      </div>
    );
  }
}

export default AddWallet;
