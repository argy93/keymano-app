import React from 'react';
import './style.scss';
import Select from 'react-dropdown-select';
import arrowImg from './images/triplearrow.svg';

class Exchange extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  render() {

    return (
      <div className="c-exchange">
        <div className="c-exchange__course">1 eur ≈ 0.000195973 BTc</div>
        <div className="c-exchange__row">
          <input type="number" placeholder="58" className="c-exchange__input" />
          <div className="c-exchange__select">
            <Select
              contentRenderer={this.customContentRenderer}
              dropdownRenderer={this.customDropdownRenderer}
              autosize={false}
            />
          </div>
        </div>
        <div className="c-exchange__convertation">
          <img src={arrowImg} />
        </div>
        <div className="c-exchange__row">
          <div className="c-exchange__input">~0.011387</div>
          <div className="c-exchange__select">
            <Select
              contentRenderer={this.customContentRendererCrypto}
              dropdownRenderer={this.customDropdownRendererCrypto}
              autosize={false}
            />
          </div>
        </div>
        <button className="c-exchange__button">Exchange</button>
      </div>
    );
  }

  customContentRendererCrypto = () => {
    return (
      <div className="c-exchange__content">
        <div className="c-exchange__item">BTC (Bitcoin)</div>
      </div>
    )
  };

  customDropdownRendererCrypto = () => {

    return (
      <div className="c-exchange__dropdown">
        <div className="c-exchange__item">Eth (Etherium)</div>
        <div className="c-exchange__item">Lit (Litecoin)</div>
      </div>
    )
  };

  customContentRenderer = () => {
    return (
      <div className="c-exchange__content">
        <div className="c-exchange__item">EUR (Euro)</div>
      </div>
    )
  };

  customDropdownRenderer = () => {

    return (
      <div className="c-exchange__dropdown">
        <div className="c-exchange__item">EUR (Euro)</div>
        <div className="c-exchange__item">USA (Dollar)</div>
        <div className="c-exchange__item">GRN (Grivna)</div>
      </div>
    )
  };
}


Exchange.propTypes = {};

export default Exchange;
