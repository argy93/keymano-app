import React, {Component} from 'react';
import './style.scss';
import PropTypes from 'prop-types';

class Button extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      disabled,
      type,
      children,
      style,
      onClick,
    } = this.props;

    return (
      <button
        onClick={onClick}
        className={`c-button is-${style || 'default'}`}
        type={type || 'button'}
        disabled={disabled}
      >
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  disabled: PropTypes.bool,
  style: PropTypes.oneOf(['default', 'filled', 'red']),
  type: PropTypes.oneOf(['submit', 'button']),
  onClick: PropTypes.func,
};

export default Button;
