import React from 'react';
import LogoIcon from "../Icons/LogoIcon/LogoIcon";
import './style.scss';
import Navigation from "../Navigation/Navigation";

class Sidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div className="c-sidebar">
        <div className="c-sidebar__logo"><LogoIcon width={'82'} height={'52'}/></div>
        <div className="c-sidebar__menu"><Navigation/></div>
      </div>
    );
  }
}

export default Sidebar;
