import React from 'react';
import { shallow } from 'enzyme';

import SignIn from '../index';

describe('<Header />', () => {
  it('should render a div', () => {
    const renderedComponent = shallow(<SignIn />);
    expect(renderedComponent.length).toEqual(1);
  });
});
