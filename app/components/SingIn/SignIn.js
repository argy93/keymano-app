import React from 'react';
import {Link} from 'react-router-dom';
import './style.scss';
import {Formik, Form} from 'formik';
import Title from '../Title';
import * as Yup from 'yup';
import FieldText from '../FieldText';
import FieldPassword from '../FieldPassword';
import Button from '../Button';
import {withCookies, useCookies} from 'react-cookie';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .max(128, 'Email to long, maximum 128 symbols')
    .email('Email not valid')
    .required('Email is required'),
  password: Yup.string()
    .min(8, 'Password must have 8 symbols')
    .max(64, 'Password to long, maximum 64 symbols')
    .matches(/^[0-9a-zA-Z($|\-#!@?)]{8,1000}$/, 'Password can contain only digits, symbols and letters')
    .required('Password is required'),
});

class SignIn extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    const {cookies} = this.props;

    return (
      <div className="c-sign-in">
        <Title>Welcome to Keymano!</Title>
        <p className="c-sign-in__text">Enter your account using your e-mail & password below.</p>
        <div className="c-sign-in__form">
          <Formik
            initialValues={{email: '', password: ''}}
            validateOnChange={true}
            validationSchema={loginSchema}
            onSubmit={(values, {setSubmitting}) => {
              setTimeout(() => {
                // setSubmitting(false);
                cookies.set('authorized', true, {path: '/'});

                setTimeout(() => {
                  window.location.reload();
                }, 300);
              }, 400);
            }}
          >
            {({isSubmitting, touched, errors}) => (
              <Form>
                <FieldText
                  type={'email'}
                  error={errors.email}
                  touched={touched.email}
                  align={'center'}
                  name={'email'}
                  placeholder={'E-mail'}
                />
                <FieldPassword
                  placeholder={'Password'}
                  name={'password'}
                  align={'center'}
                  touched={touched.password}
                  error={errors.password}
                />
                <Button
                  style={'filled'}
                  type={'submit'}
                  disabled={Boolean(isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                >
                  Log in
                </Button>

                <div className="c-sign-in__margin-row is-small">
                  <Link to='/reset-password'>Forgot your password?</Link> <br/>
                  Don’t have an account? <Link to='/sign-up'>Create one now.</Link>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

SignIn.propTypes = {};

export default withCookies(SignIn);
