import React from 'react';
import './style.scss';
import User from "../User/User";
import PropTypes from "prop-types";
import RoundedPlusIcon from "../Icons/RoundedPlusIcon/RoundedPlusIcon";
import PageName from '../PageName';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {

    return (
      <div className="c-header">
        <div className="c-header__pname"><PageName display={true}/></div>
        <div className="c-header__user"><User/></div>
      </div>
    );
  }
}

export default Header;
