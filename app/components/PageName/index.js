import {connect} from 'react-redux';
import {compose} from 'redux';
import injectReducer from '../../utils/injectReducer';
import PageName from './PageName';
import reducer from './reducer';
import {createStructuredSelector} from 'reselect';
import {makeSelectPageName} from './selectors';

const mapDispatchToProps = (dispatch) => ({
  changePageName: (name) => dispatch({
    type: 'CHANGE_PAGE_NAME',
    name,
  }),
});

const mapStateToProps = createStructuredSelector({
  pageName: makeSelectPageName(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({key: 'global', reducer});

export default compose(withReducer, withConnect)(PageName);
export {mapDispatchToProps};
