import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';

export default class PageName extends Component {
  constructor(props) {
    super(props);
    const {children, changePageName} = props;

    if (this.props.children) {
      const name = children.toString();
      changePageName(name);
    }
  }

  render() {
    const {display, pageName} = this.props;

    if (display) {
      return (<Fragment>{pageName}</Fragment>);
    } else {
      return null;
    }
  }
}

PageName.propTypes = {
  changePageName: PropTypes.func,
  display: PropTypes.bool,
  pageName: PropTypes.string,
};
