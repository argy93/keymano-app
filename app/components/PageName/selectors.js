import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const makeSelectPageName = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('pageName')
);

export {
  makeSelectPageName,
};
