import {fromJS} from 'immutable';

// The initial state of the App
const initialState = fromJS({
  pageName: '',
});

function pageNameReducer(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_PAGE_NAME':
      return state.set('pageName', action.name);
    default:
      return state;
  }
}

export default pageNameReducer;
