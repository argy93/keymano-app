import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CheckIcon from '../Icons/CheckIcon';
import StopIcon from '../Icons/StopIcon';
import './style.scss'

class PasswordAttention extends Component {
  constructor(props) {
    super(props);

    this.strengthLevel = [
      (<div className="is-red">Too weak😓</div>),
      (<div className="is-orange">You can do better😉</div>),
      (<div className="is-blue">Strong enough💪</div>),
    ];
  }

  testPassword() {
    const {password} = this.props;

    const result = {
      length: false,
      digit: false,
      upperAndLower: false,
      successCounter: 0,
    };

    if (!password) return result;

    if (password.length >= 8) {
      result.length = true;
      result.successCounter += 1;
    }

    if (/[0-9]/.test(password)) {
      result.digit = true;
      result.successCounter += 1;
    }

    if (/[a-z]/.test(password) && /[A-Z]/.test(password)) {
      result.upperAndLower = true;
      result.successCounter += 1;
    }

    return result;
  }

  render() {
    const {
      length,
      digit,
      upperAndLower,
      successCounter,
    } = this.testPassword();

    let strengthMessage = this.strengthLevel[0];

    if (successCounter > 1 && successCounter < 3) {
      strengthMessage = this.strengthLevel[1];
    } else if (successCounter === 3) {
      strengthMessage = this.strengthLevel[2];
    }

    return (
      <div className="c-password-attention">
        <div className="c-password-attention__head">
          <b>PASSWORD STRENGTH:</b> {strengthMessage}
        </div>
        <div className="c-password-attention__content">
          <p>Password should contain:</p>
          <ul>
            <li>{length ? (<CheckIcon/>) : (<StopIcon/>)} 8 symbols or more</li>
            <li>{digit ? (<CheckIcon/>) : (<StopIcon/>)} At least one digit</li>
            <li>{upperAndLower ? (<CheckIcon/>) : (<StopIcon/>)} One upper and lowercase</li>
          </ul>
        </div>
      </div>
    );
  }
}

PasswordAttention.propTypes = {
  password: PropTypes.string,
};

export default PasswordAttention;
