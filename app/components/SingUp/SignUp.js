import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import './style.scss';
import {Formik, Form} from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import FieldText from '../FieldText';
import Button from '../Button';
import Title from '../Title';
import FieldPassword from '../FieldPassword';
import LoadingIndicator from '../LoadingIndicator';
import Error from '../Error';

const step1Schema = Yup.object().shape({
  firstName: Yup.string()
    .max(64, 'First name to large, maximum 64 symbols')
    .required('First name is required'),
  lastName: Yup.string()
    .max(64, 'Last name to large, maximum 64 symbols')
    .required('Last name is required'),
});

const step2Schema = Yup.object().shape({
  email: Yup.string()
    .max(128, 'Email to long, maximum 128 symbols')
    .email('Email not valid')
    .required('Email is required'),
  password: Yup.string()
    .min(8, 'Password must have 8 symbols')
    .max(1000, 'Password to long, maximum 1000 symbols')
    .matches(/^[0-9a-zA-Z($|\-#!@?)]{8,1000}$/, 'Password can contain only digits, symbols and letters')
    .required('Password is required'),
  confirmPassword: Yup.string()
    .required('Confirm password is required')
    .oneOf([Yup.ref('password')], 'Passwords must match'),
});

class SignUp extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      confirmVisible: false,
    }
  }

  render() {
    const {
      resendEmail,
      submitStep,
      changeStep,
      submitRegistration,
      step,
      formData,
      formData: {
        firstName,
        lastName,
        email,
        password,
        confirmPassword,
      },
      sendingData,
      error,
    } = this.props;

    const {
      confirmVisible,
    } = this.state;

    return (
      <div className="c-sign-up">
        {step === 3 ? (
          <div className="c-sign-up__status u-bold u-upper">Account registration</div>
        ) : (
          <div className="c-sign-up__status u-bold u-upper">Account registration. step {step}/3</div>
        )}
        <div className="c-sign-up__form">
          {step === 1 ? (
            <Fragment>
              <Title>
                Welcome to Keymano! <br/>
                Introduce yourself, please.
              </Title>
              <Formik
                initialValues={{firstName, lastName}}
                validateOnChange={true}
                validationSchema={step1Schema}
                onSubmit={(values, {setSubmitting}) => {
                  const {firstName, lastName} = values;

                  submitStep({
                    ...formData,
                    firstName,
                    lastName,
                  });

                  changeStep(2)
                }}
              >
                {({isSubmitting, touched, errors}) => (
                  <Form
                    className={'is-step-1'}
                  >
                    <FieldText
                      placeholder="First Name"
                      name="firstName"
                      touched={touched.firstName}
                      error={errors.firstName}
                      align={'center'}
                    />

                    <FieldText
                      placeholder="Last Name"
                      name="lastName"
                      touched={touched.lastName}
                      error={errors.lastName}
                      align={'center'}
                    />

                    <Button
                      type={'submit'}
                      style={'filled'}
                      disabled={Boolean(isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                    >
                      Next
                    </Button>

                    <div className="c-sign-up__another-way">
                      Already have an account? <Link to='/sign-in'>Sign in</Link>
                    </div>
                  </Form>
                )}
              </Formik>
            </Fragment>
          ) : null}
          {step === 2 ? (
            <Fragment>
              <Title>
                Enter your e-mail and choose password <br/>
                to use your Keymano account
              </Title>
              <Formik
                initialValues={{email, password, confirmPassword}}
                validateOnChange={true}
                validationSchema={step2Schema}
                onSubmit={(values, {setSubmitting}) => {
                  const {email, password, confirmPassword} = values;

                  submitStep({
                    ...formData,
                    email,
                    password,
                    confirmPassword,
                  });

                  submitRegistration();

                  setSubmitting(false);
                }}
              >
                {({isSubmitting, touched, errors}) => (
                  <Form
                    className={`is-step-2 ${sendingData ? 'is-loading' : ''}`}
                  >
                    {sendingData ? (
                      <LoadingIndicator/>
                    ) : null }

                    <FieldText
                      placeholder="E-mail"
                      name="email"
                      touched={touched.email}
                      error={errors.email}
                      align={'center'}
                    />

                    <FieldPassword
                      placeholder="Password"
                      name="password"
                      touched={touched.password}
                      error={errors.password}
                      align={'center'}
                      withAttention={true}
                      onUpdate={(event) => {
                        if (event.target.value.length >= 8) {
                          this.setState({
                            confirmVisible: true,
                          });
                        } else {
                          this.setState({
                            confirmVisible: false,
                          });
                        }
                      }}
                    />

                    <FieldPassword
                      placeholder="Confirm password"
                      name="confirmPassword"
                      touched={touched.confirmPassword}
                      error={errors.confirmPassword}
                      align={'center'}
                      className={confirmVisible ? '' : ' is-hidden'}
                    />

                    {error ? (
                      <Error>
                        Sorry, something went wrong. Try again later.
                      </Error>
                    ) : null}

                    <div className="c-sign-up__buttons">
                      <Button
                        onClick={() => changeStep(1)}
                      >
                        Previous step
                      </Button>

                      <Button
                        style="filled"
                        type="submit"
                        disabled={Boolean(isSubmitting || !Object.keys(touched).length || Object.keys(errors).length)}
                      >
                        Next
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </Fragment>
          ) : null}
          {step === 3 ? (
            <Fragment>
              <Title>Only one step left!</Title>
              <div className="c-sign-up__final-message">
                Check out your e-mail.<br/>
                We’ve send you verification letter<br/>
                to {email}
              </div>
              <div className="c-sign-up__another-way">
                Haven’t received e-mail?<br/>
                Check your Spam folder or <a onClick={() => resendEmail(email)} href="javascript:">resend e-mail</a>
              </div>
            </Fragment>
          ) : null}
        </div>
      </div>
    );
  }
}

SignUp.propTypes = {
  resendEmail: PropTypes.func,
  changeStep: PropTypes.func,
  submitStep: PropTypes.func,
  submitRegistration: PropTypes.func,
  step: PropTypes.number,
  formData: PropTypes.object,
  sendingData: PropTypes.bool,
  error: PropTypes.bool,
};

export default SignUp;
