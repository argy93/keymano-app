import React from 'react';
import {NavLink, withRouter} from 'react-router-dom';
import './style.scss';
import routes from '../../utils/routes';

class Navigation extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.navigation = routes;
  }

  render() {
    return (
      <div className="c-navigation">
        {this.navigation.map(item => {
          return Navigation.getNavigationItem(item);
        })}
      </div>
    );
  }


  static getNavigationItem(item, mod='') {
    mod = 'c-navigation__item '+mod;

    return (
      <NavLink className={mod} key={item.path} to={item.path} activeClassName={'is-active'} exact={item.path === '/' }>
        <span className={"c-navigation__icon"}>{item.icon}</span>
        <span className={"c-navigation__text"}>{item.name}</span>
      </NavLink>
    );
  }
}

const NavigationWithRouter = withRouter(props => <Navigation {...props}/>);

export default NavigationWithRouter;
