import React from 'react';
import UserIcon from "../Icons/UserIcon/UserIcon";
import EditIcon from "../Icons/EditIcon/EditIcon";
import LogoutIcon from "../Icons/LogoutIcon/LogoutIcon";
import './style.scss';

class Profile extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="c-profile">
        <div className="c-profile__head">
          <div className="c-profile__head-icon">
            <UserIcon width={'100%'} height={'auto'} />
          </div>
          <div className="c-profile__head-info">
            <p>Panteleimon Konstantinopolskiy</p>
            <EditIcon />
          </div>
        </div>
        <div className="c-profile__body">
          <div className="c-profile__body-title">
            <span>Personal info</span>
            <EditIcon />
          </div>
          <div className="c-data-block">
            <span className="c-data-block__label">Username</span>
            <span className="c-data-block__info">Panteleimonk</span>
          </div>
          <div className="c-data-block">
            <span className="c-data-block__label">E-mail</span>
            <span className="c-data-block__info">panteleimonk@kmn.com</span>
          </div>
          <div className="c-profile__body-logout">
            <LogoutIcon />
            <span>Log out</span>
          </div>
        </div>
      </div>
    );
  }
}


Profile.propTypes = {};

export default Profile;
