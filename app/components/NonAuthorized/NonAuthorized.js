import React, {Component} from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import LogoIcon from '../Icons/LogoIcon';

class NonAuthorized extends Component {
  render() {
    const {children} = this.props;

    return (
      <div className="c-non-authorized">
        <div className="c-non-authorized__head">
          <LogoIcon width="145" height="90"/>
        </div>
        <div className="c-non-authorized__content">
          {children}
        </div>
      </div>
    );
  }
}

export default NonAuthorized;
