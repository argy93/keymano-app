import React from 'react';
import BitcoinIcon from "../components/Icons/BitcoinIcon/BitcoinIcon";
import EtheriumIcon from "../components/Icons/EtheriumIcon/EtheriumIcon";
import RippleIcon from "../components/Icons/RippleIcon/RippleIcon";
import LitecoinIcon from "../components/Icons/LitecoinIcon/LitecoinIcon";
import EosioIcon from "../components/Icons/EosioIcon/EosioIcon";

class WalletType {
  constructor(name, short, icon) {
    this.name = name;
    this.short = short;
    this.icon = icon;
  }

  getData() {
    const {name, short, icon} = this;

    return {
      name, short, icon
    }
  }

  resizeIcon(type) {
    return React.cloneElement(
      this.icon,
      {type}
    )
  }
}

const types = {
  bitcoin: new WalletType('Bitcoin', 'Btc', (<BitcoinIcon/>)),
  etherium: new WalletType('Etherium', 'Eth', (<EtheriumIcon/>)),
  ripple: new WalletType('Ripple', 'Xpr', (<RippleIcon/>)),
  litecoin: new WalletType('Litecoin', 'Ltc', (<LitecoinIcon/>)),
  eosio: new WalletType('Eosio', 'Eos', (<EosioIcon/>))
};

export default class WalletTypes {
  static getTypes() {
    return types;
  }
}
