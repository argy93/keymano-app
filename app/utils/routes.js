import React from 'react';
import CardsIcon from '../components/Icons/CardsIcon';
import WalletIcon from '../components/Icons/WalletIcon';
import SettingIcon from '../components/Icons/SettingIcon';

export default [
  {
    icon: (<CardsIcon width={'24'} height={'24'}/>),
    path: '/',
    name: 'Card'
  },
  {
    icon: (<WalletIcon width={'26'} height={'24'}/>),
    path: '/wallet',
    name: 'Wallet'
  },
  {
    icon: (<SettingIcon width={'24'} height={'24'}/>),
    path: '/settings',
    name: 'Settings'
  },
]
