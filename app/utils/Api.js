import axios from 'axios';

export default class Api {
  static sendRegistrationData(data) {
    // TODO: axios request
    return new Promise((resolve, reject) => {
      console.log('Api.sendRegistrationData', data);
      setTimeout(() => {
        resolve();
        // reject();
      }, 2000);
    });
  }

  static sendCardOrderFormData(data) {
    // TODO: axios request
    return new Promise((resolve, reject) => {
      console.log('Api.sendCardOrderFormData', data);
      setTimeout(() => {
        resolve();
        // reject();
      }, 2000);
    });
  }

  static getBitstampCurrencies() {
    return new Promise((resolve, reject) => {
      axios.all([
        axios.get('https://www.bitstamp.net/api/v2/ticker_hour/btcusd/'),
        axios.get('https://www.bitstamp.net/api/v2/ticker_hour/btceur/'),
        axios.get('https://www.bitstamp.net/api/v2/ticker_hour/eurusd/'),
      ]).then(axios.spread((btcusd, btceur, eurusd) => {
        resolve({
          btcusd: parseFloat(btcusd.data.last),
          btceur: parseFloat(btceur.data.last),
          eurusd: parseFloat(eurusd.data.last),
        });
      })).catch(reject);
    });
  }
}
