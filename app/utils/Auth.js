import Cookies from 'universal-cookie';

export default class Auth {

  static isAuthorized() {
    const cookies = new Cookies();

    return Boolean(cookies.get('authorized'));
  }
}
