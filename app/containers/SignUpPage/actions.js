import {
  CHANGE_STEP,
  RESEND_EMAIL,
  SEND_REGISTRATION_DATA, SEND_REGISTRATION_DATA_ERROR,
  SEND_REGISTRATION_DATA_SUCCESS,
  SUBMIT_REGISTRATION,
  SUBMIT_STEP,
} from './constants';

export function changeStep(step) {
  return {
    type: CHANGE_STEP,
    step,
  };
}

export function submitStep(formData) {
  return {
    type: SUBMIT_STEP,
    formData,
  };
}

export function resendEmail() {
  return {
    type: RESEND_EMAIL,
  };
}

export function submitRegistration() {
  return {
    type: SUBMIT_REGISTRATION,
  };
}

export function sendRegistrationData() {
  return {
    type: SEND_REGISTRATION_DATA,
  };
}

export function sendRegistrationDataSuccess() {
  return {
    type: SEND_REGISTRATION_DATA_SUCCESS,
  };
}

export function sendRegistrationDataError() {
  return {
    type: SEND_REGISTRATION_DATA_ERROR,
  };
}
