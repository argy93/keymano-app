import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import reducer from './reducer';
import {
  changeStep,
  resendEmail, submitRegistration,
  submitStep,
} from './actions';
import SignUpPage from './SignUpPage';
import saga from './saga';
import {
  makeSelectError,
  makeSelectFormData,
  makeSelectSendingData,
  makeSelectStep,
} from './selector';

const mapDispatchToProps = (dispatch) => ({
  resendEmail: () => dispatch(resendEmail()),
  changeStep: (step) => dispatch(changeStep(step)),
  submitStep: (formData) => dispatch(submitStep(formData)),
  submitRegistration: () => dispatch(submitRegistration()),
});

const mapStateToProps = createStructuredSelector({
  step: makeSelectStep(),
  formData: makeSelectFormData(),
  sendingData: makeSelectSendingData(),
  error: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'signUp', reducer});
const withSaga = injectSaga({key: 'signUp', saga});

export default compose(withReducer, withSaga, withConnect)(SignUpPage);
export {mapDispatchToProps};
