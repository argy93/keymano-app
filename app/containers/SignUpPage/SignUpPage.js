/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import './style.scss';
import SignUp from "../../components/SingUp";

export default class SignUpPage extends React.Component {// eslint-disable-line react/prefer-stateless-function

  render() {
    const {
      resendEmail,
      changeStep,
      submitStep,
      submitRegistration,
      step,
      formData,
      sendingData,
      error,
    } = this.props;

    return (
        <React.Fragment>
          <Helmet>
            <title>Sign up</title>
            <meta name="description" content="Sign-up page keymano" />
          </Helmet>
          <SignUp
            resendEmail={resendEmail}
            changeStep={changeStep}
            submitStep={submitStep}
            step={step}
            formData={formData}
            submitRegistration={submitRegistration}
            sendingData={sendingData}
            error={error}
          />
        </React.Fragment>
    );
  }
}

SignUpPage.propTypes = {
  resendEmail: PropTypes.func,
  changeStep: PropTypes.func,
  submitStep: PropTypes.func,
  submitRegistration: PropTypes.func,
  step: PropTypes.number,
  formData: PropTypes.object,
  sendingData: PropTypes.bool,
  error: PropTypes.bool,
};
