import { createSelector } from 'reselect';

const selectSingUp = (state) => state.get('signUp');

const makeSelectStep = () => createSelector(
  selectSingUp,
  (signUp) => signUp.get('step'),
);

const makeSelectFormData = () => createSelector(
  selectSingUp,
  (signUp) => signUp.get('formData').toJS(),
);

const makeSelectSendingData = () => createSelector(
  selectSingUp,
  (signUp) => signUp.get('sendingData'),
);

const makeSelectError = () => createSelector(
  selectSingUp,
  (signUp) => signUp.get('error'),
);

export {
  makeSelectStep,
  makeSelectFormData,
  makeSelectSendingData,
  makeSelectError,
}
