import {fromJS, Map} from 'immutable';

import {
  CHANGE_STEP,
  SEND_REGISTRATION_DATA,
  SEND_REGISTRATION_DATA_ERROR,
  SEND_REGISTRATION_DATA_SUCCESS,
  SUBMIT_STEP,
} from './constants';

// The initial state of the SignUp page
const initialState = fromJS({
  step: 1,
  sendingData: false,
  error: false,
  formData: {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
  },
});

function signUpReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_STEP:
      return state.set('step', action.step);
    case SUBMIT_STEP:
      return state.set('formData', state.get('formData').merge(Map(action.formData)));
    case SEND_REGISTRATION_DATA:
      return state.set('sendingData', true)
        .set('error', false);
    case SEND_REGISTRATION_DATA_ERROR:
      return state.set('sendingData', false)
        .set('error', true);
    case SEND_REGISTRATION_DATA_SUCCESS:
      return state.set('sendingData', false)
        .set('step', 3);
    default:
      return state;
  }
}

export default signUpReducer;
