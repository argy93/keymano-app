import {
  takeEvery,
  select,
  put,
  call,
} from 'redux-saga/effects';
import {SUBMIT_REGISTRATION} from './constants';
import {makeSelectFormData} from './selector';
import Api from '../../utils/Api';
import {sendRegistrationData, sendRegistrationDataError, sendRegistrationDataSuccess} from './actions';

function* submitRegistration() {
  console.log('submit registration');
  const formData = yield select(makeSelectFormData());
  yield put(sendRegistrationData());
  try {
    yield call(Api.sendRegistrationData, formData);
    yield put(sendRegistrationDataSuccess());
  } catch (e) {
    console.warn(e);
    yield put(sendRegistrationDataError());
  }
}

export default function* signUpSaga() {
  yield takeEvery(SUBMIT_REGISTRATION, submitRegistration);
}
