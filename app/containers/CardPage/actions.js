/*
 * Card Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  SEND_ORDER_FORM,
  SEND_ORDER_FORM_ERROR,
  SEND_ORDER_FORM_SUCCESS,
  SHOW_ORDER_FORM,
  SUBMIT_ORDER_FORM,
} from './constants';


export function showOrderForm() {
  return {
    type: SHOW_ORDER_FORM,
  };
}

export function submitOrderForm(formData) {
  return {
    type: SUBMIT_ORDER_FORM,
    formData
  };
}

export function sendOrderForm() {
  return {
    type: SEND_ORDER_FORM,
  };
}

export function sendOrderFormSuccess() {
  return {
    type: SEND_ORDER_FORM_SUCCESS,
  };
}

export function sendOrderFormError() {
  return {
    type: SEND_ORDER_FORM_ERROR,
  };
}
