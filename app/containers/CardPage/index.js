import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import {showOrderForm, submitOrderForm} from './actions';
import {
  makeSelectCardOrdered,
  makeSelectError,
  makeSelectFormData,
  makeSelectFormSending,
  makeSelectOrderForm,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import CardPage from './CardPage';

const mapDispatchToProps = (dispatch) => ({
  showOrderForm: () => dispatch(showOrderForm()),
  submitOrderForm: (formData) => dispatch(submitOrderForm(formData))
});

const mapStateToProps = createStructuredSelector({
  orderForm: makeSelectOrderForm(),
  formSending: makeSelectFormSending(),
  error: makeSelectError(),
  cardOrdered: makeSelectCardOrdered(),
  formData: makeSelectFormData(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'card', reducer });
const withSaga = injectSaga({ key: 'card', saga });

export default compose(withReducer, withSaga, withConnect)(CardPage);
export { mapDispatchToProps };
