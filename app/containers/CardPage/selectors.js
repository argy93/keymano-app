/**
 * Card selectors
 */

import { createSelector } from 'reselect';

const selectCard = (state) => state.get('card');

const makeSelectOrderForm = () => createSelector(
  selectCard,
  (card) => card.get('orderForm'),
);

const makeSelectFormSending = () => createSelector(
  selectCard,
  (card) => card.get('formSending'),
);

const makeSelectError = () => createSelector(
  selectCard,
  (card) => card.get('error'),
);

const makeSelectCardOrdered = () => createSelector(
  selectCard,
  (card) => card.get('cardOrdered'),
);

const makeSelectFormData = () => createSelector(
  selectCard,
  (card) => card.get('formData').toJS(),
);

export {
  selectCard,
  makeSelectOrderForm,
  makeSelectFormSending,
  makeSelectError,
  makeSelectCardOrdered,
  makeSelectFormData
};
