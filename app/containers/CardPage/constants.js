export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';
export const SHOW_ORDER_FORM = 'kmn/cardPage/SHOW_ORDER_FORM';
export const SUBMIT_ORDER_FORM = 'kmn/cardPage/SUBMIT_ORDER_FORM';
export const SEND_ORDER_FORM = 'kmn/cardPage/SEND_ORDER_FORM';
export const SEND_ORDER_FORM_SUCCESS = 'kmn/cardPage/SEND_ORDER_FORM_SUCCESS';
export const SEND_ORDER_FORM_ERROR = 'kmn/cardPage/SEND_ORDER_FORM_ERROR';
