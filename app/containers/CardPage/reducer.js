/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import {fromJS, Map} from 'immutable';

import {
  SEND_ORDER_FORM,
  SEND_ORDER_FORM_ERROR,
  SEND_ORDER_FORM_SUCCESS,
  SHOW_ORDER_FORM,
  SUBMIT_ORDER_FORM,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  orderForm: false,
  formSending: false,
  error: false,
  cardOrdered: false,
  formData: {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    street: '',
    apartment: '',
    country: '',
    city: '',
    zip: '',
  },
});

function cardPageReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_ORDER_FORM:
      return state.set('orderForm', true);
    case SUBMIT_ORDER_FORM:
      return state.set('formData', state.get('formData').merge(Map(action.formData)));
    case SEND_ORDER_FORM:
      return state.set('formSending', true)
        .set('error', false);
    case SEND_ORDER_FORM_SUCCESS:
      return state.set('formSending', false)
        .set('cardOrdered', true)
        .set('orderForm', false);
    case SEND_ORDER_FORM_ERROR:
      return state.set('formSending', false)
        .set('error', true);
    default:
      return state;
  }
}

export default cardPageReducer;
