/**
 * Gets the repositories of the user from Github
 */

import {
  call, put, select, takeEvery,
} from 'redux-saga/effects';
import {SUBMIT_ORDER_FORM} from './constants';
import {makeSelectFormData} from './selectors';
import {sendOrderForm, sendOrderFormError, sendOrderFormSuccess} from './actions';
import Api from '../../utils/Api';

function* submitOrderForm() {
  console.log('test');
  const data = yield select(makeSelectFormData());
  yield put(sendOrderForm());
  try {
    yield call(Api.sendCardOrderFormData, data);
    yield put(sendOrderFormSuccess());
  } catch (e) {
    yield put(sendOrderFormError());
  }
}

export default function* cardPageSaga() {
  yield takeEvery(SUBMIT_ORDER_FORM, submitOrderForm);
}
