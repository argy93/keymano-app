/*
 * CardPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
import './style.scss';
import OrderCard from "../../components/OrderCard";
import PageName from '../../components/PageName';

export default class CardPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      showOrderForm,
      submitOrderForm,
      orderForm,
      formSending,
      error,
      cardOrdered,
      formData
    } = this.props;

    return (
      <section className="p-home">
        <Helmet>
          <title>Card</title>
          <meta name="description" content="Keymano - card page"/>
        </Helmet>
        <PageName>Card</PageName>
        <OrderCard
          showOrderForm={showOrderForm}
          submitOrderForm={submitOrderForm}
          orderForm={orderForm}
          formSending={formSending}
          error={error}
          cardOrdered={cardOrdered}
          formData={formData}
        />
      </section>
    );
  }
}

CardPage.propTypes = {
  showOrderForm: PropTypes.func,
  submitOrderForm: PropTypes.func,
  orderForm: PropTypes.bool,
  formSending: PropTypes.bool,
  error: PropTypes.bool,
  cardOrdered: PropTypes.bool,
  formData: PropTypes.object,
};
