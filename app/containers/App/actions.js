/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  LOAD_REPOS_SUCCESS,
  LOAD_REPOS,
  LOAD_REPOS_ERROR,
  SIGN_IN_LOGIN_CHANGE,
  SIGN_IN_PASSWORD_CHANGE,
  SIGN_IN_TFA_CHANGE, LOAD_CURRENCIES, LOAD_CURRENCIES_SUCCESS, LOAD_CURRENCIES_ERROR,
} from './constants';

/**
 * Load the repositories, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadRepos() {
  return {
    type: LOAD_REPOS,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function reposLoaded(repos, username) {
  return {
    type: LOAD_REPOS_SUCCESS,
    repos,
    username,
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function repoLoadingError(error) {
  return {
    type: LOAD_REPOS_ERROR,
    error,
  };
}

/**
 * Dispatched when sign in login input change
 *
 * @param {string} login
 *
 * @returns {object}
 */
export function signInLoginChange(login) {
  return {
    type: SIGN_IN_LOGIN_CHANGE,
    login,
  };
}

/**
 * Dispatched when sign in password change
 *
 * @param {string} password

 * @returns {object}
 */
export function signInPasswordChange(password) {
  return {
    type: SIGN_IN_PASSWORD_CHANGE,
    password,
  };
}

/**
 * Dispatched when sign in 2FA button click
 *
 * @param {string} tfa

 * @returns {object}
 */
export function signInTfaChange(tfa) {
  return {
    type: SIGN_IN_TFA_CHANGE,
    tfa,
  };
}
