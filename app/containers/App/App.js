import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Switch, Route, Redirect} from 'react-router-dom';

import './style.scss';
import Auth from '../../utils/Auth';
import CardPage from '../CardPage/Loadable';
import WalletPage from '../WalletPage/Loadable';
import SignInPage from '../SignInPage/Loadable';
import SignUpPage from '../SignUpPage/Loadable';
import SignRestorePage from '../SignRestorePage/Loadable';
import SettingsPage from '../SettingsPage/Loadable';
import NonAuthorized from '../../components/NonAuthorized';
import Dashboard from '../../components/Dashboard';
class App extends Component {
  render() {
    // useCookies(['authorized']);

    return (
      <div className="app-wrapper">
        {Auth.isAuthorized() ? (
          <Fragment>
            <Helmet
              titleTemplate="%s - Keymano"
              defaultTitle="Keymano"
            >
              <meta name="description" content="Keymano"/>
            </Helmet>
            <Dashboard>
              <Switch>
                <Route exact path="/" component={CardPage}/>
                <Route path="/wallet" component={WalletPage}/>/>
                <Route exact path="/settings" component={SettingsPage}/>
                <Redirect to="/"/>
              </Switch>
            </Dashboard>
          </Fragment>
        ) : (
          <NonAuthorized>
            <Helmet
              titleTemplate="%s - Keymano"
              defaultTitle="Keymano"
            >
              <meta name="description" content="Keymano"/>
            </Helmet>

            <Switch>
              <Route exact path="/sign-in" component={SignInPage}/>
              <Route exact path="/sign-up" component={SignUpPage}/>
              <Route exact path="/reset-password" component={SignRestorePage}/>
              <Redirect to="/sign-in"/>
            </Switch>
          </NonAuthorized>
        )}
      </div>
    );
  }
}

export default App;
