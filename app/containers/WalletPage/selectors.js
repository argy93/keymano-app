/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectWallet = (state) => state.get('wallet');

const makeSelectCurrencies = () => createSelector(
  selectWallet,
  (walletState) => walletState.get('currencies').toJS(),
);

const makeSelectLoadCurrencies = () => createSelector(
  selectWallet,
  (walletState) => walletState.get('loadCurrencies'),
);

const makeSelectLoadCurrenciesError = () => createSelector(
  selectWallet,
  (walletState) => walletState.get('loadCurrenciesError'),
);

const makeSelectLoadCurrenciesErrorMessage = () => createSelector(
  selectWallet,
  (walletState) => walletState.get('loadCurrenciesErrorMessage'),
);

const makeSelectWallets = () => createSelector(
  selectWallet,
  (walletState) => {
    return walletState.get('wallets').toJS()
  },
);

const makeSelectFilterType = () => createSelector(
  selectWallet,
  (walletState) => {
    return walletState.get('filterType');
  },
);

export {
  makeSelectCurrencies,
  makeSelectWallets,
  makeSelectFilterType,
  makeSelectLoadCurrencies,
  makeSelectLoadCurrenciesError,
  makeSelectLoadCurrenciesErrorMessage
};
