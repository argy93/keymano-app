/*
 * Wallet
 *
 * This is the first thing users see of our App, at the '/wallet' route
 */

import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import './style.scss';
import PropTypes from 'prop-types';
import WalletTypes from '../../utils/walletTypes';
import Overview from '../../components/Overview';
import Wallet from '../../components/Wallet';
import DdWallet from '../../components/DdWallets';
import WalletExpanded from '../../components/WalletExpanded';
import WalletInfoControl from '../../components/WalletInfoControl';
import BackIcon from '../../components/Icons/BackIcon';
import Transactions from '../../components/Transactions/Transactions';
import ReceiveLog from '../../components/ReceiveLog/ReceiveLog';
import Send from '../../components/Send';
import PageName from '../../components/PageName';
import {Switch, Route} from 'react-router-dom';
import Filters from '../../utils/Filters';
import Link from 'react-router-dom/Link';

export default class WalletPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      onCardsClick,
      onShortClick,
      loadCurrencies,
      wallets,
      filterType,
      loadingCurrencies,
      loadCurrenciesError,
      loadCurrenciesErrorMessage,
      currencies
    } = this.props;

    return (
      <section className="p-wallet">
        <Helmet>
          <title>Wallet</title>
          <meta name="description" content="Keymano - Wallet page"/>
        </Helmet>
        <div className="l-wallets">
          <Switch>
            <Route exact path="/wallet">
              <div className="l-wallets__main-list">
                <PageName>Wallet</PageName>
                <Overview
                  title='Overview'
                  onCardsClick={onCardsClick}
                  onShortClick={onShortClick}
                  active={filterType}/>
                <div className={'l-wallets__list' + (filterType === 'short' ? ' is-short' : '')}>
                  {wallets.map(wallet => (
                    <Wallet
                      to={`/wallet/${wallet.type}`}
                      id={wallet.id}
                      code={wallet.key}
                      type={wallet.type}
                      key={wallet.type}
                      date={wallet.date}
                      eq={wallet.eq}
                      fee={wallet.fee}
                      shift={wallet.shift}
                      summary={wallet.summary}
                      mode={filterType}
                    />
                  ))}
                </div>
              </div>
            </Route>
            <Route
              path="/wallet/:walletName"
              render={(
                {
                  match: {
                    params: {
                      walletName,
                    },
                  },
                  history,
                }) => {
                const picked = wallets.find((wallet) => wallet.type === walletName);
                const pickWalletId = picked.id;

                return (
                  <Fragment>
                    <Helmet>
                      <title>{`${Filters.capitalize(walletName)} wallet`}</title>
                      <meta name="description" content="Keymano - Wallet page"/>
                    </Helmet>
                    <PageName>{Filters.capitalize(walletName)}</PageName>
                    <div className="l-wallets__detail">
                      <div className="l-wallets__sidebar">
                        <div className="l-wallets__head">
                          <Link className="u-link is-simple" to="/wallet"><BackIcon/> <span>Back</span></Link>
                          <DdWallet active={pickWalletId} options={wallets} onChange={(id) => {
                            history.push(`/wallet/${wallets.find((wallet) => wallet.id === id).type}`);
                          }}/>
                        </div>
                        <WalletExpanded
                          code={picked.key}
                          summary={picked.summary}
                          eq={picked.eq}
                          type={picked.type}
                        />
                        <Route
                          path="/wallet/:walletName/:filterType?"
                          render={(
                            {
                              match: {
                                params: {
                                  walletName,
                                  filterType = 'receive',
                                },
                              },
                              history,
                            }) => (
                            <WalletInfoControl
                              filterType={filterType}
                              changeFilterType={(filterType) => {
                                history.push(`/wallet/${picked.type}/${filterType}`);
                              }}
                            />
                          )}
                        />
                      </div>
                      <div className="l-wallets__content">
                        <Route
                          path="/wallet/:walletName/:filterType?"
                          render={(
                            {
                              match: {
                                params: {
                                  filterType = 'receive',
                                },
                              },
                            }) => (
                            <div className="l-wallets__head">
                              <div className="l-wallets__content-name">{filterType}</div>
                            </div>
                          )}
                        />
                        <Switch>
                          <Route strict path={'/wallet/:walletName/send'}>
                            <Send
                              loadCurrencies={loadCurrencies}
                              loadingCurrencies={loadingCurrencies}
                              loadCurrenciesError={loadCurrenciesError}
                              loadCurrenciesErrorMessage={loadCurrenciesErrorMessage}
                              currencies={currencies}
                            />
                          </Route>
                          <Route
                            strict
                            path={'/wallet/:walletName/transactions/:filterType?/:page?'}
                            render={(
                              {
                                match: {
                                  params: {
                                    walletName,
                                    filterType = 'all',
                                    page = 1
                                  },
                                },
                                history,
                              }) => {

                              return (
                                <Transactions
                                  filterType={filterType}
                                  onChangeFilterType={(type) => {
                                    history.push(`/wallet/${walletName}/transactions/${type}`);
                                  }}
                                  onPageChange={(page) => {
                                    history.push(`/wallet/${walletName}/transactions/${filterType}/${page + 1}`);
                                  }}
                                  page={page - 1}
                                  transactions={picked.transactions}
                                />
                              );
                            }}
                          />
                          <Route
                            path={'/wallet/:walletName/(receive)?/:page?'}
                            render={(
                              {
                                match: {
                                  params: {
                                    walletName,
                                    filterType = 'receive',
                                    page = 1,
                                  },
                                },
                              }) => (
                              <ReceiveLog
                                onPageChange={(page) => {
                                  history.push(`/wallet/${walletName}/${filterType}/${page + 1}`);
                                }}
                                page={page - 1}
                                items={picked.receive}/>
                            )}
                          />
                        </Switch>
                      </div>
                    </div>
                  </Fragment>
                );
              }}/>
          </Switch>
        </div>
      </section>
    );
  }
}

WalletPage.propTypes = {
  onCardsClick: PropTypes.func,
  onShortClick: PropTypes.func,
  loadCurrencies: PropTypes.func,
  wallets: PropTypes.array,
  filterType: PropTypes.string,
  loadingCurrencies: PropTypes.bool,
  loadCurrenciesError: PropTypes.bool,
  loadCurrenciesErrorMessage: PropTypes.string,
  currencies: PropTypes.object,
};
