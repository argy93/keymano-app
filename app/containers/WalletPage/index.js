import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import WalletPage from './WalletPage';
import reducer from './reducer';
import saga from './saga';
import {
  filterCards,
  filterShort,
  loadCurrencies,
} from './actions';
import {
  makeSelectCurrencies,
  makeSelectFilterType,
  makeSelectLoadCurrencies,
  makeSelectLoadCurrenciesError,
  makeSelectLoadCurrenciesErrorMessage,
  makeSelectWallets,
} from './selectors';

const mapDispatchToProps = (dispatch) => ({
  onCardsClick: () => dispatch(filterCards()),
  onShortClick: () => dispatch(filterShort()),
  loadCurrencies: () => dispatch(loadCurrencies()),
});

const mapStateToProps = createStructuredSelector({
  wallets: makeSelectWallets(),
  filterType: makeSelectFilterType(),
  loadingCurrencies: makeSelectLoadCurrencies(),
  loadCurrenciesError: makeSelectLoadCurrenciesError(),
  loadCurrenciesErrorMessage: makeSelectLoadCurrenciesErrorMessage(),
  currencies: makeSelectCurrencies(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'wallet', reducer});
const withSaga = injectSaga({key: 'wallet', saga});

export default compose(withReducer, withSaga, withConnect)(WalletPage);
export {mapDispatchToProps};
