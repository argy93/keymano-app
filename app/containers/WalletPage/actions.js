/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  FILTER_CARDS, FILTER_SHORT, PICK_WALLET, DROP_PICKED_WALLET, CHANGE_WALLET_INFO_CONTROL_TYPE,
  CHANGE_TRANSACTION_PAGE, CHANGE_TRANSACTIONS_TYPE, CHANGE_RECEIVE_PAGE,
  LOAD_CURRENCIES, LOAD_CURRENCIES_ERROR, LOAD_CURRENCIES_SUCCESS,
} from './constants';

export function pickWallet(wallet) {
  return {
    type: PICK_WALLET,
    wallet,
  };
}

export function filterShort() {
  return {
    type: FILTER_SHORT,
  };
}

export function filterCards() {
  return {
    type: FILTER_CARDS,
  };
}

export function onDropPickedWallet() {
  return {
    type: DROP_PICKED_WALLET,
  };
}

export function onChangeWalletInfoControlType(walletInfoType) {
  return {
    type: CHANGE_WALLET_INFO_CONTROL_TYPE,
    walletInfoType,
  };
}

export function onChangeTransactionPage(transactionId, page) {
  return {
    type: CHANGE_TRANSACTION_PAGE,
    transactionId,
    page,
  };
}

export function onChangeReceivePage(transactionId, page) {
  return {
    type: CHANGE_RECEIVE_PAGE,
    transactionId,
    page,
  };
}

export function onChangeTransactionsType(transactionId, transactionsType) {
  return {
    type: CHANGE_TRANSACTIONS_TYPE,
    transactionId,
    transactionsType,
  };
}

export function loadCurrencies() {
  return {
    type: LOAD_CURRENCIES,
  };
}

export function loadCurrenciesSuccess(currencies) {
  return {
    type: LOAD_CURRENCIES_SUCCESS,
    currencies,
  };
}

export function loadCurrenciesError(message) {
  return {
    type: LOAD_CURRENCIES_ERROR,
    message
  };
}
