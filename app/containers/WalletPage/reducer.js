/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import {fromJS, Map} from 'immutable';

import {
  CHANGE_RECEIVE_PAGE,
  CHANGE_TRANSACTION_PAGE,
  CHANGE_TRANSACTIONS_TYPE,
  CHANGE_WALLET_INFO_CONTROL_TYPE,
  DROP_PICKED_WALLET,
  FILTER_CARDS,
  FILTER_SHORT,
  LOAD_CURRENCIES,
  LOAD_CURRENCIES_ERROR,
  LOAD_CURRENCIES_SUCCESS,
  PICK_WALLET,
} from './constants';
import walletsList from './static-data/wallets-list';

// The initial state of the App
const initialState = fromJS({
  filterType: 'cards',
  wallets: walletsList,
  loadCurrencies: false,
  loadCurrenciesError: false,
  loadCurrenciesErrorMessage: '',
  currencies: {
    btcusd: 0,
    btceur: 0,
    eurusd: 0
  }
});

function walletPageReducer(state = initialState, action) {
  let index;

  switch (action.type) {
    case FILTER_CARDS:
      return state.set('filterType', 'cards');
    case FILTER_SHORT:
      return state.set('filterType', 'short');
    case PICK_WALLET:
      return state.set('pickWallet', action.wallet);
    case DROP_PICKED_WALLET:
      return state.set('pickWallet', false);
    case CHANGE_WALLET_INFO_CONTROL_TYPE:
      return state.set('walletInfoType', action.walletInfoType);
    case CHANGE_TRANSACTION_PAGE:
      index = state.get('wallets').findIndex(wallet => wallet.get('id') === action.transactionId);
      return state.setIn(['wallets', index, 'page'], action.page);
    case CHANGE_RECEIVE_PAGE:
      index = state.get('wallets').findIndex(wallet => wallet.get('id') === action.transactionId);
      return state.setIn(['wallets', index, 'receivePage'], action.page);
    case CHANGE_TRANSACTIONS_TYPE:
      index = state.get('wallets').findIndex(wallet => wallet.get('id') === action.transactionId);
      return state.setIn(['wallets', index, 'filter'], action.transactionsType)
        .setIn(['wallets', index, 'page'], 0);
    case LOAD_CURRENCIES:
      return state.set('loadCurrencies', true)
        .set('loadCurrenciesError', false);
    case LOAD_CURRENCIES_SUCCESS:
      return state.set('currencies', state.get('currencies').merge(Map(action.currencies)))
        .set('loadCurrencies', false);
    case LOAD_CURRENCIES_ERROR:
      return state.set('loadCurrencies', false)
        .set('loadCurrenciesError', true)
        .set('loadCurrenciesErrorMessage', action.errorMessage);
    default:
      return state;
  }
}

export default walletPageReducer;
