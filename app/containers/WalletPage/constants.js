/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';
export const PICK_WALLET = 'kmn/Wallets/PICK_WALLET';
export const FILTER_CARDS = 'kmn/Wallets/FILTER_CARDS';
export const FILTER_SHORT = 'kmn/Wallets/FILTER_SHORT';
export const DROP_PICKED_WALLET = 'kmn/Wallets/DROP_PICKED_WALLET';
export const CHANGE_WALLET_INFO_CONTROL_TYPE = 'kmn/Wallets/CHANGE_WALLET_INFO_CONTROL_TYPE';
export const CHANGE_TRANSACTION_PAGE = 'kmn/Wallets/CHANGE_TRANSACTION_PAGE';
export const CHANGE_RECEIVE_PAGE = 'kmn/Wallets/CHANGE_RECEIVE_PAGE';
export const CHANGE_TRANSACTIONS_TYPE = 'kmn/Wallets/CHANGE_TRANSACTION_TYPE';

export const LOAD_CURRENCIES = 'kmn/App/LOAD_CURRENCIES';
export const LOAD_CURRENCIES_SUCCESS = 'kmn/App/LOAD_CURRENCIES_SUCCESS';
export const LOAD_CURRENCIES_ERROR = 'kmn/App/LOAD_CURRENCIES_ERROR';
