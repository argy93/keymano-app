export default [
  {
    id: 1,
    type: 'bitcoin',
    summary: 100,
    eq: 646.34,
    date: 1564673937128,
    shift: 1.045,
    key: '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
    fee: 0.01,
    transactions: generateTransactions(),
    receive: generateReceive(),
  },
  {
    id: 2,
    type: 'etherium',
    summary: 85.02,
    eq: 646.34,
    date: 1564673937128,
    shift: 1.045,
    key: '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
    fee: 0.01,
    transactions: generateTransactions(),
    receive: generateReceive(),
  },
  {
    id: 3,
    type: 'ripple',
    summary: 23,
    eq: 646.34,
    date: 1564673937128,
    shift: 1.045,
    key: '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
    fee: 0.01,
    transactions: generateTransactions(),
    receive: generateReceive(),
  },
  {
    id: 4,
    type: 'litecoin',
    summary: 257.48,
    eq: 646.34,
    date: 1564673937128,
    shift: 1.045,
    key: '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
    fee: 0.01,
    transactions: generateTransactions(),
    receive: generateReceive(),
  },
  {
    id: 5,
    type: 'eosio',
    summary: 29.8,
    eq: 646.34,
    date: 1564673937128,
    shift: 1.045,
    key: '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
    fee: 0.01,
    transactions: generateTransactions(),
    receive: generateReceive(),
  },
];

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateTransactions() {
  const types = ['deposit', 'withdraw'];
  const status = ['success', 'error'];
  const now = new Date();
  const transactionsCount = getRandomInt(20, 90);
  const result = [];
  const fromCards = [
    '226483559563875918236271253904280',
    '221239897862352529403725514888826',
    '991283655129049727512892076256282',
    '200283216382198321687366265555522',
    '657218362615321637890238265268011',
    '213978624672893929377256178290383',
    '213092186712632783271653896712111',
    '362489127380293872873652127334985',
    '827364620923121276312783982732777',
    '666666666666666666666666666666666',
  ];

  for (let i = 0; i <= transactionsCount; i++) {
    result.push({
      id: i,
      type: types[getRandomInt(0, 1)],
      status: status[getRandomInt(0, 1)],
      date: now.setDate(now.getDate() - i),
      amount: getRandomInt(-100, 1000),
      fee: 0.01,
      from: [fromCards[getRandomInt(0, 9)]],
      to: [
        '226483559563875910000000000000000',
      ],
    })
  }

  return result;
}

function generateReceive() {
  const result = [];
  const receiveCount = getRandomInt(20, 90);

  for (let i = 0; i <= receiveCount; i++) {
    result.push({
      id: i,
      status: 'Recieved',
      address: '2264835595638759100000000000000000'
    });
  }

  return result;
}
