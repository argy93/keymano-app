/**
 * Gets the repositories of the user from Github
 */

import {
  call, put, select, takeLatest,
} from 'redux-saga/effects';
import {LOAD_CURRENCIES} from './constants';
import {makeSelectCurrencies} from './selectors';
import {loadCurrenciesError, loadCurrenciesSuccess} from './actions';
import Api from '../../utils/Api';

/**
 * Github repos request/response handler
 */
export function* loadCurrencies() {
  const currencies = yield select(makeSelectCurrencies());
  if (currencies.btcusd !== 0) {
    yield put(loadCurrenciesSuccess(currencies));
  } else {
    try {
      const currencies = yield call(Api.getBitstampCurrencies);
      yield put(loadCurrenciesSuccess(currencies));
    } catch (e) {
      console.log(e);
      yield put(loadCurrenciesError(e.toString()));
    }
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {

  yield takeLatest(LOAD_CURRENCIES, loadCurrencies);
}
