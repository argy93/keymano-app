import {connect} from 'react-redux';
import {compose} from 'redux';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import ExchangePage from './ExchangePage';
import {} from './actions';

const mapDispatchToProps = (dispatch) => ({

});

const mapStateToProps = (state) => {

  return state.get('exchange').toJS();
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'exchange', reducer});

export default compose(withReducer, withConnect)(ExchangePage);
export {mapDispatchToProps};
