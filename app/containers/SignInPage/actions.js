import {
  SEND_LOGIN_FORM_DATA,
  SEND_LOGIN_FORM_DATA_ERROR,
  SEND_LOGIN_FORM_DATA_SUCCESS,
  SUBMIT_LOGIN_FORM,
} from './constants';

export function submitLoginForm(formData) {
  return {
    type: SUBMIT_LOGIN_FORM,
    formData
  };
}

export function sendLoginFormData() {
  return {
    type: SEND_LOGIN_FORM_DATA,
  };
}

export function sendLoginFormDataSuccess() {
  return {
    type: SEND_LOGIN_FORM_DATA_SUCCESS,
  };
}

export function sendLoginFormDataError(errors) {
  return {
    type: SEND_LOGIN_FORM_DATA_ERROR,
    errors
  };
}
