import {fromJS, Map} from 'immutable';

import {
  SEND_LOGIN_FORM_DATA,
  SEND_LOGIN_FORM_DATA_ERROR,
  SEND_LOGIN_FORM_DATA_SUCCESS,
  SUBMIT_LOGIN_FORM,
  TFA_CHANGE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  sendingLoginForm: false,
  error: false,
  errors: {
    wrongLoginData: false,
    fatal: false
  },
  formData: {
    email: '',
    password: ''
  }
});

function signInReducer(state = initialState, action) {
  switch (action.type) {
    case SUBMIT_LOGIN_FORM:
      return state.set('formData', state.get('formData').merge(Map(action.formData)));
    case SEND_LOGIN_FORM_DATA:
      return state.set('sendingLoginForm', true)
        .state.set('error', false);
    case SEND_LOGIN_FORM_DATA_ERROR:
      return state.set('sendingLoginForm', false)
        .state.set('error', true)
        .state.set('errors', state.get('errors').merge(Map(action.errors)));
    default:
      return state;
  }
}

export default signInReducer;
