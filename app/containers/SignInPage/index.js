import {connect} from "react-redux";
import {compose} from "redux";
import SignInPage from "./SignInPage";
import injectReducer from '../../utils/injectReducer';
import reducer from "./reducer";
import { withCookies } from 'react-cookie';

const mapDispatchToProps = (dispatch) => ({
    buttonClick: (value) => dispatch(tfaButtonClick(value))
});

const mapStateToProps = (state) => {
    return {
        ...state.getIn(['signIn']).toJS()
    }
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'signIn', reducer });

export default compose(withReducer, withConnect)(SignInPage);
export { mapDispatchToProps };
