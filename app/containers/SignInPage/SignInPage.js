/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import {Helmet} from 'react-helmet';
import './style.scss';
import SignIn from '../../components/SingIn';

export default class SignInPage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Sign in</title>
          <meta name="description" content="A React.js Boilerplate application homepage"/>
        </Helmet>
        <SignIn/>
      </React.Fragment>
    );
  }
}
