import {createSelector} from 'reselect';

const selectSignRestore = (state) => state.get('signRestore');

const makeSelectRestoreSending = () => createSelector(
  selectSignRestore,
  (signRestore) => signRestore.get('restoreSending'),
);

const makeSelectRestoreSendingError = () => createSelector(
  selectSignRestore,
  (signRestore) => signRestore.get('restoreSendingError'),
);

const makeSelectRestoreSendingErrorMessage = () => createSelector(
  selectSignRestore,
  (signRestore) => signRestore.get('restoreSendingErrorMessage'),
);

const makeSelectRestoreSendingSuccess = () => createSelector(
  selectSignRestore,
  (signRestore) => signRestore.get('restoreSendingSuccess'),
);

const makeSelectFormData = () => createSelector(
  selectSignRestore,
  (signRestore) => signRestore.get('formData').toJS(),
);

export {
  makeSelectRestoreSending,
  makeSelectRestoreSendingError,
  makeSelectRestoreSendingErrorMessage,
  makeSelectRestoreSendingSuccess,
  makeSelectFormData
}
