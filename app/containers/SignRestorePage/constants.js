export const SEND_RESTORE_PASSWORD = 'kmn/SignRestorePage/SEND_RESTORE_PASSWORD';
export const SEND_RESTORE_PASSWORD_SUCCESS = 'kmn/SignRestorePage/SEND_RESTORE_SUCCESS';
export const SEND_RESTORE_PASSWORD_ERROR = 'kmn/SignRestorePage/SEND_RESTORE_ERROR';
