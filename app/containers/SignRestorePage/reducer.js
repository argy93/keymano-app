import {fromJS} from 'immutable';

import {
  SEND_RESTORE_PASSWORD,
  SEND_RESTORE_PASSWORD_ERROR,
  SEND_RESTORE_PASSWORD_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  restoreSending: false,
  restoreSendingError: false,
  restoreSendingErrorMessage: '',
  restoreSendingSuccess: false,
  formData: {
    email: ''
  }
});

function signRestoreReducer(state = initialState, action) {
  switch (action.type) {
    case SEND_RESTORE_PASSWORD:
      return state.set('restoreSending', true)
        .set('restoreSendingError', false)
        .setIn(['formData', 'email'], action.email);
    case SEND_RESTORE_PASSWORD_SUCCESS:
      return state.set('restoreSendingSuccess', true)
        .set('restoreSending', true);
    case SEND_RESTORE_PASSWORD_ERROR:
      return  state.set('restoreSending', false)
        .set('restoreSendingError', true)
        .set('restoreSendingErrorMessage', action.message);
    default:
      return state;
  }
}

export default signRestoreReducer;
