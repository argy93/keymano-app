import {connect} from "react-redux";
import {compose} from "redux";
import SignRestorePage from "./SignRestorePage";
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import reducer from "./reducer";
import {createStructuredSelector} from 'reselect';
import {
  makeSelectFormData,
  makeSelectRestoreSending,
  makeSelectRestoreSendingError,
  makeSelectRestoreSendingErrorMessage,
  makeSelectRestoreSendingSuccess,
} from './selectors';
import {sendRestorePassword} from './actions';
import saga from './saga';

const mapDispatchToProps = (dispatch) => ({
  sendRestorePassword: (email) => dispatch(sendRestorePassword(email))
});

const mapStateToProps = createStructuredSelector({
  restoreSending: makeSelectRestoreSending(),
  restoreSendingError: makeSelectRestoreSendingError(),
  restoreSendingErrorMessage: makeSelectRestoreSendingErrorMessage(),
  restoreSendingSuccess: makeSelectRestoreSendingSuccess(),
  formData: makeSelectFormData(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'signRestore', reducer });
const withSaga = injectSaga({key: 'signRestore', saga});

export default compose(withReducer, withSaga, withConnect)(SignRestorePage);
export { mapDispatchToProps };
