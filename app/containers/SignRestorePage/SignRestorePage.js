import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types'
import './style.scss';
import SignRestore from '../../components/SingRestore';

export default class SignRestorePage extends React.Component {// eslint-disable-line react/prefer-stateless-function

  render() {
    const {
      sendRestorePassword,
      restoreSending,
      restoreSendingError,
      restoreSendingErrorMessage,
      restoreSendingSuccess,
      formData
    } = this.props;

    return (
        <React.Fragment>
          <Helmet>
            <title>Restore password</title>
            <meta name="description" content="A React.js Boilerplate application homepage" />
          </Helmet>
          <SignRestore
            sendRestorePassword={sendRestorePassword}
            restoreSending={restoreSending}
            restoreSendingError={restoreSendingError}
            restoreSendingErrorMessage={restoreSendingErrorMessage}
            restoreSendingSuccess={restoreSendingSuccess}
            formData={formData}
          />
        </React.Fragment>
    );
  }
}

SignRestorePage.propTypes = {
  sendRestorePassword: PropTypes.func,
  restoreSending: PropTypes.bool,
  restoreSendingError: PropTypes.bool,
  restoreSendingErrorMessage: PropTypes.string,
  restoreSendingSuccess: PropTypes.bool,
  formData: PropTypes.object,
};
