import {
  takeEvery,
  delay,
  put,
} from 'redux-saga/effects';

import {SEND_RESTORE_PASSWORD} from './constants';
import {sendRestorePasswordSuccess} from './actions';

function* sendRestorePassword() {
  // yield delay(300);
  yield put(sendRestorePasswordSuccess());
}

export default function* singRestoreSaga() {
  yield takeEvery(SEND_RESTORE_PASSWORD, sendRestorePassword);
}
