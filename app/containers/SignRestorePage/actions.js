import {
  SEND_RESTORE_PASSWORD,
  SEND_RESTORE_PASSWORD_ERROR,
  SEND_RESTORE_PASSWORD_SUCCESS
} from './constants';

export function sendRestorePassword(email) {
  return {
    type: SEND_RESTORE_PASSWORD,
    email,
  };
}

export function sendRestorePasswordError(message) {
  return {
    type: SEND_RESTORE_PASSWORD_ERROR,
    message,
  };
}

export function sendRestorePasswordSuccess() {
  return {
    type: SEND_RESTORE_PASSWORD_SUCCESS,
  };
}
