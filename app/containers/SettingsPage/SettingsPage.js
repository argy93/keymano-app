/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import {Helmet} from 'react-helmet';
import './style.scss';
import phoneImage from './images/phone.svg';
import phone9Image from './images/phone9.svg';
import shiedlImage from './images/shield.svg';
import PropTypes from 'prop-types';
import Sidebar from '../../components/Sidebar';
import TfaToggler from '../../components/TfaToggler';
import Header from '../../components/Header';
import TotpSetup from '../../components/TotpSetup';
import PaperSetup from '../../components/PaperSetup';
import PageName from '../../components/PageName';

// eslint-disable-next-line react/prefer-stateless-function
export default class SettingsPage extends React.Component {
  render() {
    const {tfaType, onTfaToggleClick} = this.props;

    return (
      <section className="p-settings">
        <Helmet>
          <title>Settings</title>
          <meta name="description" content="Keymano - Settings page"/>
        </Helmet>
        <PageName>Settings</PageName>
        <div className="l-dashboard">
          Page In development
        </div>
      </section>
    );
  }

}

SettingsPage.propTypes = {
  tfaType: PropTypes.oneOf(['totp', 'paper', 'none']),
  onTfaToggleClick: PropTypes.func,
};
