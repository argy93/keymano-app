import {connect} from 'react-redux';
import {compose} from 'redux';
import SettingsPage from './SettingsPage';
import {tfaChange} from './actions';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';

const mapDispatchToProps = (dispatch) => ({
  onTfaToggleClick: (tfaType) => dispatch(tfaChange(tfaType)),
});

const mapStateToProps = (state) => {
  return state.get('settings').toJS();
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'settings', reducer});

export default compose(withReducer, withConnect)(SettingsPage);
export {mapDispatchToProps};
