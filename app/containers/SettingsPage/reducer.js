import {fromJS} from 'immutable';
import {SETTINGS_TFA_CHANGE} from './constants';

// The initial state of the App
const initialState = fromJS({
  tfaType: 'none',
});

function settingsReducer(state = initialState, action) {
  switch (action.type) {
    case SETTINGS_TFA_CHANGE:
      return state.set('tfaType', action.typeName);
    default:
      return state;
  }
}

export default settingsReducer;
