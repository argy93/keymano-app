import {connect} from 'react-redux';
import {compose} from 'redux';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import ProfilePage from './ProfilePage';
import {} from './actions';

const mapDispatchToProps = (dispatch) => ({

});

const mapStateToProps = (state) => {

  return state.get('profile').toJS();
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'profile', reducer});

export default compose(withReducer, withConnect)(ProfilePage);
export {mapDispatchToProps};
