/*
 * Wallet
 *
 * This is the first thing users see of our App, at the '/wallet' route
 */

import React from 'react';
import {Helmet} from 'react-helmet';
import './style.scss';
import PropTypes from 'prop-types';
import Sidebar from '../../components/Sidebar';
import Header from '../../components/Header';
import WalletTypes from '../../utils/walletTypes';
import Exchange from '../../components/Exchange/Exchange';
import Profile from '../../components/Profile/Profile';
import RoundedPlusIcon from '../../components/Icons/RoundedPlusIcon/RoundedPlusIcon';

export default class ProfilePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.types = WalletTypes.getTypes();
  }

  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {

  }

  render() {
    const {

    } = this.props;

    return (
      <section className="p-profile">
        <Helmet>
          <title>Profile Page</title>
          <meta name="description" content="Keymano - Profile page" />
        </Helmet>
        <div className="l-dashboard">
          <Sidebar/>
          <div className="l-dashboard__content">
            <Header pageName="User profile">
              <div className="l-header__account">
                <RoundedPlusIcon />
                <span>Add new account</span>
              </div>
            </Header>
            <Profile/>
          </div>
        </div>
      </section>
    );
  }
}

ProfilePage.propTypes = {};
