/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */
import './utils/dateFormat';
// Needed for redux-saga es6 generator support
import '@babel/polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import FontFaceObserver from 'fontfaceobserver';
import createHistory from 'history/createBrowserHistory';
import 'sanitize.css/sanitize.css';
import {CookiesProvider} from 'react-cookie';

// Import root app
import App from './containers/App';

// Load the favicon
/* eslint-disable import/no-webpack-loader-syntax */
import '!file-loader?name=[name].[ext]!./images/android-icon-36x36.png';
import '!file-loader?name=[name].[ext]!./images/android-icon-48x48.png';
import '!file-loader?name=[name].[ext]!./images/android-icon-72x72.png';
import '!file-loader?name=[name].[ext]!./images/android-icon-96x96.png';
import '!file-loader?name=[name].[ext]!./images/android-icon-144x144.png';
import '!file-loader?name=[name].[ext]!./images/android-icon-192x192.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-57x57.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-60x60.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-72x72.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-76x76.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-114x114.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-120x120.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-144x144.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-152x152.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-180x180.png';
import '!file-loader?name=[name].[ext]!./images/apple-icon-precomposed.png';
import '!file-loader?name=[name].[ext]!./images/browserconfig.xml';
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import '!file-loader?name=[name].[ext]!./images/favicon-16x16.png';
import '!file-loader?name=[name].[ext]!./images/favicon-32x32.png';
import '!file-loader?name=[name].[ext]!./images/favicon-96x96.png';
import './images/manifest.json';
import '!file-loader?name=[name].[ext]!./images/ms-icon-70x70.png';
import '!file-loader?name=[name].[ext]!./images/ms-icon-144x144.png';
import '!file-loader?name=[name].[ext]!./images/ms-icon-150x150.png';
import '!file-loader?name=[name].[ext]!./images/ms-icon-310x310.png';
/* eslint-enable import/no-webpack-loader-syntax */

// Import CSS reset and Global Styles
import 'styles/theme.scss';

import configureStore from './configureStore';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Mukta', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

// Create redux store with history
const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

const render = () => {
  ReactDOM.render(
    <CookiesProvider>
      <Provider store={store}>
        {/* <LanguageProvider messages={messages}> */}
        <ConnectedRouter history={history}>
          <App/>
        </ConnectedRouter>
        {/* </LanguageProvider> */}
      </Provider>
    </CookiesProvider>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}

render();
